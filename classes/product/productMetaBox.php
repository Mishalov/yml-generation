<?php
	
class YMLProductMetaBox
{

	static function run()
	{
		add_action( 'woocommerce_product_write_panel_tabs', array( 'YMLProductMetaBox', 'add_product_yml_generation_panel_tab') );
		add_action( 'woocommerce_product_data_panels',  array( 'YMLProductMetaBox', 'add_product_yml_generation_panel_data' ) );

		if( !class_exists( 'YMLProductMetaBoxSettings' ) ):
			include_once( YML_PLUGIN_PATH . 'classes/product/productMetaBoxSettings.php' );
		endif;

		YMLProductMetaBoxSettings::run();
	}

	static function add_product_yml_generation_panel_tab()
	{
		?>
		<li class="yml_generation_options yml_generation_tab">
			<a href="#yml_generation_product_data">Yandex Generetion</a>
		</li>
		<?php
	}

	static function add_product_yml_generation_panel_data()
	{
		if( file_exists( $metaBoxViews = YML_PLUGIN_PATH . 'views/MetaBoxProduct/metaBox.php' ) ):
			include_once( $metaBoxViews );
		endif;
	}

	public static function getYML_product_options( $postID )
	{
		$output = array();
		$visibility = get_post_meta( $postID, '_yml_veiw_product', true );
		$default = get_post_meta( $postID, '_yml_default_option', true );

		if( !isset($visibility) || $visibility != 'yes' ){

			if( !isset($default) || $default != 'yes' ){
				return get_option('yml_default_product_settings');
			}else{
				$output['type']                          = get_post_meta( $postID, '_yml_type', true );
				$output['currencyId']                    = get_post_meta( $postID, '_yml_currencyId', true );
				$output['description']['get']            = get_post_meta( $postID, '_yml_description', true );
				$output['description']['myInput']        = get_post_meta( $postID, '_yml_description_myInput', true );
				$output['sales_notes']['get']            = get_post_meta( $postID, '_yml_sales_notes', true );
				$output['sales_notes']['myInput']        = get_post_meta( $postID, '_yml_sales_notes_myInput', true );
				$output['vendor']['get']                 = get_post_meta( $postID, '_yml_vendor', true );
				$output['vendor']['attribute']           = get_post_meta( $postID, '_yml_vendor_attribute', true );
				$output['vendor']['myInput']             = get_post_meta( $postID, '_yml_vendor_myInput', true );
				$output['model']['get']                  = get_post_meta( $postID, '_yml_model', true );
				$output['model']['attribute']            = get_post_meta( $postID, '_yml_model_attribute', true );
				$output['model']['myInput']              = get_post_meta( $postID, '_yml_model_myInput', true );
				$output['vendorCode']['get']             = get_post_meta( $postID, '_yml_vendorCode', true );
				$output['vendorCode']['attribute']       = get_post_meta( $postID, '_yml_vendorCode_attribute', true );
				$output['vendorCode']['myInput']         = get_post_meta( $postID, '_yml_vendorCode_myInput', true );
				$output['typePrefix']['get']             = get_post_meta( $postID, '_yml_typePrefix', true );
				$output['typePrefix']['attribute']       = get_post_meta( $postID, '_yml_typePrefix_attribute', true );
				$output['typePrefix']['myInput']         = get_post_meta( $postID, '_yml_typePrefix_myInput', true );
				$output['author']['get']                 = get_post_meta( $postID, '_yml_author', true );
				$output['author']['attribute']           = get_post_meta( $postID, '_yml_author_attribute', true );
				$output['author']['myInput']             = get_post_meta( $postID, '_yml_author_myInput', true );
				$output['author']['get']                 = get_post_meta( $postID, '_yml_author', true );
				$output['author']['attribute']           = get_post_meta( $postID, '_yml_author_attribute', true );
				$output['author']['myInput']             = get_post_meta( $postID, '_yml_author_myInput', true );
				$output['publisher']['get']              = get_post_meta( $postID, '_yml_publisher', true );
				$output['publisher']['attribute']        = get_post_meta( $postID, '_yml_publisher_attribute', true );
				$output['publisher']['myInput']          = get_post_meta( $postID, '_yml_publisher_myInput', true );
				$output['series']['get']                 = get_post_meta( $postID, '_yml_series', true );
				$output['series']['attribute']           = get_post_meta( $postID, '_yml_series_attribute', true );
				$output['series']['myInput']             = get_post_meta( $postID, '_yml_series_myInput', true );
				$output['year']['get']                   = get_post_meta( $postID, '_yml_year', true );
				$output['year']['attribute']             = get_post_meta( $postID, '_yml_year_attribute', true );
				$output['year']['myInput']               = get_post_meta( $postID, '_yml_year_myInput', true );
				$output['ISBN']['get']                   = get_post_meta( $postID, '_yml_ISBN', true );
				$output['ISBN']['attribute']             = get_post_meta( $postID, '_yml_ISBN_attribute', true );
				$output['ISBN']['myInput']               = get_post_meta( $postID, '_yml_ISBN_myInput', true );
				$output['volume']['get']                 = get_post_meta( $postID, '_yml_volume', true );
				$output['volume']['attribute']           = get_post_meta( $postID, '_yml_volume_attribute', true );
				$output['volume']['myInput']             = get_post_meta( $postID, '_yml_volume_myInput', true );
				$output['part']['get']                   = get_post_meta( $postID, '_yml_part', true );
				$output['part']['attribute']             = get_post_meta( $postID, '_yml_part_attribute', true );
				$output['part']['myInput']               = get_post_meta( $postID, '_yml_part_myInput', true );
				$output['language']['get']               = get_post_meta( $postID, '_yml_language', true );
				$output['language']['attribute']         = get_post_meta( $postID, '_yml_language_attribute', true );
				$output['language']['myInput']           = get_post_meta( $postID, '_yml_language_myInput', true );
				$output['binding']['get']                = get_post_meta( $postID, '_yml_binding', true );
				$output['binding']['attribute']          = get_post_meta( $postID, '_yml_binding_attribute', true );
				$output['binding']['myInput']            = get_post_meta( $postID, '_yml_binding_myInput', true );
				$output['page_extent']['get']            = get_post_meta( $postID, '_yml_page_extent', true );
				$output['page_extent']['attribute']      = get_post_meta( $postID, '_yml_page_extent_attribute', true );
				$output['page_extent']['myInput']        = get_post_meta( $postID, '_yml_page_extent_myInput', true );
				$output['performed_by']['get']           = get_post_meta( $postID, '_yml_performed_by', true );
				$output['performed_by']['attribute']     = get_post_meta( $postID, '_yml_performed_by_attribute', true );
				$output['performed_by']['myInput']       = get_post_meta( $postID, '_yml_performed_by_myInput', true );
				$output['storage']['get']                = get_post_meta( $postID, '_yml_storage', true );
				$output['storage']['attribute']          = get_post_meta( $postID, '_yml_storage_attribute', true );
				$output['storage']['myInput']            = get_post_meta( $postID, '_yml_storage_myInput', true );
				$output['format']['get']                 = get_post_meta( $postID, '_yml_format', true );
				$output['format']['attribute']           = get_post_meta( $postID, '_yml_format_attribute', true );
				$output['format']['myInput']             = get_post_meta( $postID, '_yml_format_myInput', true );
				$output['recording_length']['get']       = get_post_meta( $postID, '_yml_recording_length', true );
				$output['recording_length']['attribute'] = get_post_meta( $postID, '_yml_recording_length_attribute', true );
				$output['recording_length']['myInput']   = get_post_meta( $postID, '_yml_recording_length_myInput', true );
				$output['artist']['get']                 = get_post_meta( $postID, '_yml_artist', true );
				$output['artist']['attribute']           = get_post_meta( $postID, '_yml_artist_attribute', true );
				$output['artist']['myInput']             = get_post_meta( $postID, '_yml_artist_myInput', true );
				$output['media']['get']                  = get_post_meta( $postID, '_yml_media', true );
				$output['media']['attribute']            = get_post_meta( $postID, '_yml_media_attribute', true );
				$output['media']['myInput']              = get_post_meta( $postID, '_yml_media_myInput', true );
				$output['starring']['get']               = get_post_meta( $postID, '_yml_starring', true );
				$output['starring']['attribute']         = get_post_meta( $postID, '_yml_starring_attribute', true );
				$output['starring']['myInput']           = get_post_meta( $postID, '_yml_starring_myInput', true );
				$output['director']['get']               = get_post_meta( $postID, '_yml_director', true );
				$output['director']['attribute']         = get_post_meta( $postID, '_yml_director_attribute', true );
				$output['director']['myInput']           = get_post_meta( $postID, '_yml_director_myInput', true );
				$output['originalName']['get']           = get_post_meta( $postID, '_yml_originalName', true );
				$output['originalName']['attribute']     = get_post_meta( $postID, '_yml_originalName_attribute', true );
				$output['originalName']['myInput']       = get_post_meta( $postID, '_yml_originalName_myInput', true );
				$output['country']['get']                = get_post_meta( $postID, '_yml_country', true );
				$output['country']['attribute']          = get_post_meta( $postID, '_yml_country_attribute', true );
				$output['country']['myInput']            = get_post_meta( $postID, '_yml_country_myInput', true );
				$output['param_ETK']['get']              = get_post_meta( $postID, '_yml_param_ETK', true );
				$output['param_ETK']['attribute']        = get_post_meta( $postID, '_yml_param_ETK_attribute', true );
				$output['param_ETK']['myInput']          = get_post_meta( $postID, '_yml_param_ETK_myInput', true );
				$output['place']['get']                  = get_post_meta( $postID, '_yml_place', true );
				$output['place']['attribute']            = get_post_meta( $postID, '_yml_place_attribute', true );
				$output['place']['myInput']              = get_post_meta( $postID, '_yml_place_myInput', true );
				$output['hall']['get']                   = get_post_meta( $postID, '_yml_hall', true );
				$output['hall']['attribute']             = get_post_meta( $postID, '_yml_hall_attribute', true );
				$output['hall']['myInput']               = get_post_meta( $postID, '_yml_hall_myInput', true );
				$output['hall_part']['get']              = get_post_meta( $postID, '_yml_hall_part', true );
				$output['hall_part']['attribute']        = get_post_meta( $postID, '_yml_hall_part_attribute', true );
				$output['hall_part']['myInput']          = get_post_meta( $postID, '_yml_hall_part_myInput', true );
				$output['date']['get']                   = get_post_meta( $postID, '_yml_date', true );
				$output['date']['attribute']             = get_post_meta( $postID, '_yml_date_attribute', true );
				$output['date']['myInput']               = get_post_meta( $postID, '_yml_date_myInput', true );
				$output['worldRegion']['get']            = get_post_meta( $postID, '_yml_worldRegion', true );
				$output['worldRegion']['attribute']      = get_post_meta( $postID, '_yml_worldRegion_attribute', true );
				$output['worldRegion']['myInput']        = get_post_meta( $postID, '_yml_worldRegion_myInput', true );
				$output['region']['get']                 = get_post_meta( $postID, '_yml_region', true );
				$output['region']['attribute']           = get_post_meta( $postID, '_yml_region_attribute', true );
				$output['region']['myInput']             = get_post_meta( $postID, '_yml_region_myInput', true );
				$output['days']['get']                   = get_post_meta( $postID, '_yml_days', true );
				$output['days']['attribute']             = get_post_meta( $postID, '_yml_days_attribute', true );
				$output['days']['myInput']               = get_post_meta( $postID, '_yml_days_myInput', true );
				$output['dataTour']['get']               = get_post_meta( $postID, '_yml_dataTour', true );
				$output['dataTour']['attribute']         = get_post_meta( $postID, '_yml_dataTour_attribute', true );
				$output['dataTour']['myInput']           = get_post_meta( $postID, '_yml_dataTour_myInput', true );
				$output['hotel_stars']['get']            = get_post_meta( $postID, '_yml_hotel_stars', true );
				$output['hotel_stars']['attribute']      = get_post_meta( $postID, '_yml_hotel_stars_attribute', true );
				$output['hotel_stars']['myInput']        = get_post_meta( $postID, '_yml_hotel_stars_myInput', true );
				$output['transport']['get']              = get_post_meta( $postID, '_yml_transport', true );
				$output['transport']['attribute']        = get_post_meta( $postID, '_yml_transport_attribute', true );
				$output['transport']['myInput']          = get_post_meta( $postID, '_yml_transport_myInput', true );
				$output['cbid']['get']                   = get_post_meta( $postID, '_yml_cbid', true );
				$output['cbid']['attribute']             = get_post_meta( $postID, '_yml_cbid_attribute', true );
				$output['cbid']['myInput']               = get_post_meta( $postID, '_yml_cbid_myInput', true );
				$output['bid']['get']                    = get_post_meta( $postID, '_yml_bid', true );
				$output['bid']['attribute']              = get_post_meta( $postID, '_yml_bid_attribute', true );
				$output['bid']['myInput']                = get_post_meta( $postID, '_yml_bid_myInput', true );
				$output['fee']['get']                    = get_post_meta( $postID, '_yml_fee', true );
				$output['fee']['attribute']              = get_post_meta( $postID, '_yml_fee_attribute', true );
				$output['fee']['myInput']                = get_post_meta( $postID, '_yml_fee_myInput', true );
				$output['country_of_origin']['get']      = get_post_meta( $postID, '_yml_country_of_origin', true );
				$output['country_of_origin']['attribute']= get_post_meta( $postID, '_yml_country_of_origin_attribute', true );
				$output['country_of_origin']['myInput']  = get_post_meta( $postID, '_yml_country_of_origin_myInput', true );
				$output['barcode']['get']                = get_post_meta( $postID, '_yml_barcode', true );
				$output['barcode']['attribute']          = get_post_meta( $postID, '_yml_barcode_attribute', true );
				$output['barcode']['myInput']            = get_post_meta( $postID, '_yml_barcode_myInput', true );
				$output['expiry']['get']                 = get_post_meta( $postID, '_yml_expiry', true );
				$output['expiry']['attribute']           = get_post_meta( $postID, '_yml_expiry_attribute', true );
				$output['expiry']['myInput']             = get_post_meta( $postID, '_yml_expiry_myInput', true );
				$output['extra']['delivery']             = get_post_meta( $postID, '_yml_delivery', true );
				$output['extra']['pickup']               = get_post_meta( $postID, '_yml_pickup', true );
				$output['extra']['store']                = get_post_meta( $postID, '_yml_store', true );
				$output['extra']['manufacturer_warranty']= get_post_meta( $postID, '_yml_manufacturer_warranty', true );
				$output['extra']['adult']                = get_post_meta( $postID, '_yml_adult', true );
				$output['extra']['cpa']                  = get_post_meta( $postID, '_yml_cpa', true );
				$output['extra']['weight']               = get_post_meta( $postID, '_yml_weight', true );
				$output['extra']['dimensions']           = get_post_meta( $postID, '_yml_dimensions', true );
				$output['extra']['is_premiere']          = get_post_meta( $postID, '_yml_is_premiere', true );
				$output['extra']['is_kids']              = get_post_meta( $postID, '_yml_is_kids', true );
				$output['param']                         = get_option( 'yml_default_product_settings' )['param'];

				return $output;
			}

		}else{
			array_push( $output, $visibility );
			return $output;
		}
	}
}