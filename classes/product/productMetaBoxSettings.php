<?php
	
class YMLProductMetaBoxSettings
{

	static function run()
	{

		//Save post meta
		add_action(
			'woocommerce_process_product_meta_simple',
			array( 'YMLProductMetaBoxSettings',	'save_product_yml_generation_panel_data' )
		);

		add_action(
			'woocommerce_process_product_meta_variable',
			array( 'YMLProductMetaBoxSettings', 'save_product_yml_generation_panel_data' )
		);

		add_action(
			'woocommerce_process_product_meta_grouped',
			array( 'YMLProductMetaBoxSettings',	'save_product_yml_generation_panel_data' )
		);

		add_action(
			'woocommerce_process_product_meta_external',
			array( 'YMLProductMetaBoxSettings',	'save_product_yml_generation_panel_data' )
		);

		//Register fields in metabox
		add_action(
			'yml_generation_metabox_views',
			array( 'YMLProductMetaBoxSettings', 'do_yml_type_hook')
		);

	}

	static function do_yml_type_hook()
	{
		global $post;

		if( file_exists( $settingsField = YML_PLUGIN_PATH . 'views/MetaBoxProduct/SettingFields/type.php' ) ):
			include( $settingsField );
		endif;
	}

	static function save_product_yml_generation_panel_data( $post_id )
	{
		$input = array(
						'_yml_veiw_product',
						'_yml_default_option',
						'_yml_type',
						'_yml_currencyId',
						'_yml_vendor',            '_yml_vendor_attribute',           '_yml_vendor_myInput',
						'_yml_model',             '_yml_model_attribute',            '_yml_model_myInput',
						'_yml_vendorCode',        '_yml_vendorCode_attribute',       '_yml_vendorCode_myInput',
						'_yml_cbid',              '_yml_cbid_attribute',             '_yml_cbid_myInput', 
						'_yml_bid',               '_yml_bid_attribute',              '_yml_bid_myInput',
						'_yml_fee',               '_yml_fee_attribute',              '_yml_fee_myInput',
						'_yml_country_of_origin', '_yml_country_of_origin_attribute','_yml_country_of_origin_myInput',
						'_yml_barcode',           '_yml_barcode_attribute',          '_yml_barcode_myInput',
						'_yml_description',       '_yml_description_myInput',
						'_yml_sales_notes',       '_yml_sales_notes_myInput',
						'_yml_expiry',            '_yml_expiry_attribute',           '_yml_expiry_myInput',
						'_yml_typePrefix',        '_yml_typePrefix_attribute',       '_yml_typePrefix_myInput',
						'_yml_author',            '_yml_author_attribute',           '_yml_author_myInput',
						'_yml_publisher',         '_yml_publisher_attribute',        '_yml_publisher_myInput',
						'_yml_series',            '_yml_series_attribute',           '_yml_series_myInput',
						'_yml_year',              '_yml_year_attribute',             '_yml_year_myInput',
						'_yml_ISBN',              '_yml_ISBN_attribute',             '_yml_ISBN_myInput',
						'_yml_volume',            '_yml_volume_attribute',           '_yml_volume_myInput',
						'_yml_part',              '_yml_part_attribute',             '_yml_part_myInput',
						'_yml_language',          '_yml_language_attribute',         '_yml_language_myInput',
						'_yml_binding',           '_yml_binding_attribute',          '_yml_binding_myInput',
						'_yml_page_extent',       '_yml_page_extent_attribute',      '_yml_page_extent_myInput',
						'_yml_performed_by',      '_yml_performed_by_attribute',     '_yml_performed_by_myInput',
						'_yml_storage',           '_yml_storage_attribute',          '_yml_storage_myInput',
						'_yml_format',            '_yml_format_attribute',           '_yml_format_myInput',
						'_yml_recording_length',  '_yml_recording_length_attribute', '_yml_recording_length_myInput',
						'_yml_artist',            '_yml_artist_attribute',           '_yml_artist_myInput',
						'_yml_media',             '_yml_media_attribute',            '_yml_media_myInput',
						'_yml_starring',          '_yml_starring_attribute',         '_yml_starring_myInput',
						'_yml_director',          '_yml_director_attribute',         '_yml_director_myInput',
						'_yml_originalName',      '_yml_originalName_attribute',     '_yml_originalName_myInput',
						'_yml_country',           '_yml_country_attribute',          '_yml_country_myInput',
						'_yml_param_ETK',         '_yml_param_ETK_attribute',        '_yml_param_ETK_myInput',
						'_yml_place',             '_yml_place_attribute',            '_yml_place_myInput',
						'_yml_hall',              '_yml_hall_attribute',             '_yml_hall_myInput',
						'_yml_hall_part',         '_yml_hall_part_attribute',        '_yml_hall_part_myInput',
						'_yml_date',              '_yml_date_attribute',             '_yml_date_myInput',
						'_yml_worldRegion',       '_yml_worldRegion_attribute',      '_yml_worldRegion_myInput',
						'_yml_region',            '_yml_region_attribute',           '_yml_region_myInput',
						'_yml_days',              '_yml_days_attribute',             '_yml_days_myInput',
						'_yml_dataTour',          '_yml_dataTour_attribute',         '_yml_dataTour_myInput',
						'_yml_hotel_stars',       '_yml_hotel_stars_attribute',      '_yml_hotel_stars_myInput',
						'_yml_transport',         '_yml_transport_attribute',        '_yml_transport_myInput',
						'_yml_delivery',
						'_yml_pickup',
						'_yml_store',
						'_yml_cpa',
						'_yml_is_kids',
						'_yml_adult',
						'_yml_is_premiere',
						'_yml_demensions',
						'_yml_weight',
						'_yml_manufacturer_warranty',

					 );

		foreach ($input as $value) {
			$yml_default_option = isset( $_POST[$value] ) ? $_POST[$value] : 'off';
			update_post_meta( $post_id, $value, $yml_default_option );
		}
	}

}