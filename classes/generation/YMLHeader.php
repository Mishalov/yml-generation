<?php

class YMLHeader
{
	public static function init()
	{
		$option  = get_option('yml_shop_settings');
		$header  = '';
		$header .= '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
		$header .= '<yml_catalog date="' . date('Y-m-d H:i') . '">' . PHP_EOL;
    $header .= '  <shop>' . PHP_EOL;
    $header .= '    <name>' . $option['shop_name'] . '</name>' . PHP_EOL;
    $header .= '    <company>' . $option['company_name'] . '</company>' . PHP_EOL;
    $header .= '    <url>' . SITE_URL . '</url>' . PHP_EOL;
    $header .= '    <platform>' . YML_PLATFORM . '</platform>' . PHP_EOL;
    $header .= '    <version>' . PLATFORM_VERSION . '</version>' . PHP_EOL;
    $header .= '    <agency>' . PLUGIN_AGENCY_NAME . '</agency>' . PHP_EOL;
    $header .= '    <email>' . PLUGIN_AGENCY_EMAIL . '</email>' . PHP_EOL;
    $header .= '    <currencies>' . PHP_EOL;
  foreach ($option['currencies'] as $value):
    $header .= '      <currency id="' . $value['currency'] . '" rate="';
      if( $value['rate'] == 'MyRate' ):
        $header .= $value['myRate'];
      else:
        $header .= $value['rate'];
      endif;
    $header .= '" />' . PHP_EOL;
  endforeach;
    $header .= '    </currencies>' . PHP_EOL;
    $header .= '    <categories>' . PHP_EOL;
  foreach ($option['categories'] as $value):
    $cat = get_categories( [ 'taxonomy' => 'product_cat', 'include' => $value ] );
      foreach ($cat as $value):
        if( $value->category_parent != 0 ):
          $category_parent = ' parentId="' . $value->category_parent . '"';
        endif;
    $header .= '      <category id="' . $value->cat_ID . '"' . $category_parent . '>' . $value->name . '</category>' . PHP_EOL;
      endforeach;
  endforeach;
    $header .= '    </categories>' . PHP_EOL;

  if( isset($option['deliveryShop'][0]['cost']) || isset($option['deliveryShop'][0]['days']) || isset($option['deliveryShop'][0]['order-before']) ):
    $header .= '    <delivery-options>' . PHP_EOL;
  foreach ($option['deliveryShop'] as $value):
    $cost = ( isset($value['cost']) ) ? ' cost="' . $value['cost'] . '"' : '';
    $days = ( isset($value['days']) ) ? ' days="' . $value['days'] . '"' : '';
    $order = ( isset($value['order-before']) ) ? ' order-before="' . $value['order-before'] . '"' : '';
    $header .= '      <option' . $cost . $days . $order . '/>' . PHP_EOL;
  endforeach;
    $header .= '    </delivery-options>' . PHP_EOL;
  endif;
    $header .= '    <cpa>1</cpa>' . PHP_EOL;
    //$header .= '    <offers></offers>' . PHP_EOL;
    //$header .= '  </shop>' . PHP_EOL;
    //$header .= '</yml_catalog>';

		return $header;
	}
}