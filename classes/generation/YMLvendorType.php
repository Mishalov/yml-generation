<?php

class YMLvendorType
{
	public static function init( $offer, $productOptions )
	{
		$content  = '';
		$available = ( $productOptions['extra']['delivery'] != 'off' ) ? 'true' : 'false';
    $bidValue = getDataOption( $productOptions, 'bid', $offer );
    $bid = ( $bidValue != '' && is_numeric( $bidValue ) ) ? ' bid="' . $bidValue . '"' : '';
    $cbidValue = getDataOption( $productOptions, 'cbid', $offer );
    $cbid = ( $cbidValue != '' && is_numeric( $cbidValue ) ) ? ' cbid="' . $cbidValue . '"' : '';
    $feeValue = getDataOption( $productOptions, 'fee', $offer );
    $fee = ( $feeValue != '' && is_numeric( $feeValue ) ) ? ' fee="' . $feeValue . '"' : '';
    $content .= '    <offer id="' . $offer->id . '" available="' . $available . '"' . $bid . $cbid . $fee . '/>' . PHP_EOL;
    $content .= '      <url>' . get_permalink( $offer->id ) . '</url>' . PHP_EOL;
    $content .= '      <price>' . $offer->get_price() . '</price>' . PHP_EOL;
    if( $offer->get_regular_price() > $offer->get_price() ){
      $content .= '      <oldprice>' . $offer->get_regular_price() . '</oldprice>' . PHP_EOL;
    }
    $content .= '      <currencyId>' . $productOptions['currencyId'] . '</currencyId>' . PHP_EOL;
    $content .= '      <categoryId>' . get_the_terms( $offer->id, 'product_cat' )[0]->term_id . '</categoryId>' . PHP_EOL;
    $content .= '      <picture>' . wp_get_attachment_image_url( $offer->get_image_id() ) . '</picture>' . PHP_EOL;
    //$product_gallery = $offer->get_gallery_image_ids();
    $iterator = 0;
    //foreach ($product_gallery as $key => $value) {
      //if( $iterator < 9 ):
        //$content .= '      <picture>' . wp_get_attachment_image_url( $value ) . '</picture>' . PHP_EOL;
      //endif;
      //$iterator++;
    //}
    $available = ( $productOptions['extra']['delivery'] != 'off' ) ? 'true' : 'false';
    $content .= '      <available>' . $available . '</available>' . PHP_EOL;
    $pickup = ( $productOptions['extra']['pickup'] != 'off' ) ? 'true' : 'false';
    $content .= '      <pickup>' . $pickup . '</pickup>' . PHP_EOL;
    $store = ( $productOptions['extra']['store'] != 'off' ) ? 'true' : 'false';
    $content .= '      <store>' . $store . '</store>' . PHP_EOL;
    $description = html_entity_decode( str_replace(': ', ':', getDataOption( $productOptions, 'description', $offer ) ), ENT_COMPAT, "UTF-8" );
    $content .= '      <description><![CDATA[' . $description . ']]></description>' . PHP_EOL;
    $sales_notes = strip_tags( getDataOption( $productOptions, 'sales_notes', $offer ) );
    $content .= '      <sales_notes>' . $sales_notes . '</sales_notes>' . PHP_EOL;
    $manufacturer_warranty = ( $productOptions['extra']['manufacturer_warranty'] != 'off' ) ? 'true' : 'false';
    $content .= '      <manufacturer_warranty>' . $manufacturer_warranty . '</manufacturer_warranty>' . PHP_EOL;
    $country_of_origin = getDataOption( $productOptions, 'country_of_origin', $offer );
    $country_of_origin = ( isset($country_of_origin) && $country_of_origin != '' ) ? '      <country_of_origin>' . $country_of_origin . '</country_of_origin>' . PHP_EOL : '';
    $content .= $country_of_origin;
    $adult = ( $productOptions['extra']['adult'] != 'off' ) ? 'true' : 'false';
    $content .= '      <adult>' . $adult . '</adult>' . PHP_EOL;
    $barcode = getDataOption( $productOptions, 'barcode', $offer );
    $barcode = ( $barcode != '' ) ? '      <barcode>' . $barcode . '</barcode>' . PHP_EOL : '';
    $content .= $barcode;
    $cpa = ( $productOptions['extra']['cpa'] != 'off' ) ? '1' : '0';
    $content .= '      <cpa>' . $cpa . '</cpa>' . PHP_EOL;
    $weight = $offer->get_weight();
    $weight = ( $productOptions['extra']['weight'] != 'off' && $weight > 0 ) ? '      <weight>' . $weight . '</weight>' . PHP_EOL : '';
    $content .= $weight;
    $dimensions = ( $productOptions['extra']['dimensions'] != 'off' && $offer->get_length() > 0 && $offer->get_width() > 0 && $offer->get_height() > 0 ) ? '      <dimensions>' . $offer->get_length() . '/' . $offer->get_width() . '/' . $offer->get_height() . '</dimensions>' . PHP_EOL : '';
    $content .= $dimensions;
    $downloadable = ( $offer->is_downloadable() ) ? 'true' : 'false';
    $content .= '      <downloadable>' . $downloadable . '</downloadable>' . PHP_EOL;

    foreach($productOptions['param'] as $key => $value) {
      $value = explode( '|', $value );
      $paramName = $value[1];
      $terms = wp_get_post_terms( $offer->id , 'pa_'.$value[0], array("fields" => "all") );
      foreach ($terms as $key => $value) {
        $content .= '      <param name="' . $paramName . '">' . $value->name . '</param>' . PHP_EOL;
      }
    }

    $content .= '      <rec></rec>' . PHP_EOL;

    return $content;
	}
}

if ( !function_exists('getDataOption') ):

  function getDataOption( $options, $data, $product )
  {
    $return = '';
    switch ( $options[$data]['get'] ) {
      case 'attribute':
        $attributes = $product->get_attributes();
        $return = isset( $attributes[ 'pa_' . $options[$data]['attribute'] ] ) ?
                          wp_get_post_terms( $product->id ,
                                           'pa_' . $options[$data]['attribute'],
                                            array("fields" => "all")
                                            )[0]->name : '';
        return $return;
        break;

      case 'myInput':
        $return .= $options[$data]['myInput'];
        return $return;
        break;

      case 'description':
        $return .= $product->post->post_content;
        return $return;
        break;

      case 'excerpt':
        $return .= $product->post->post_excerpt;
        return $return;
        break;
      
      default:
        return $return;
        break;
    }
  }

endif;