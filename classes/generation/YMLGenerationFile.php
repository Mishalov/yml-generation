<?php

class YMLGenerationFile
{
	public static function init()
	{
		$option  = get_option('yml_shop_settings');
    
    if( !class_exists('YMLHeader') ){
      include_once( YML_PLUGIN_PATH . 'classes/generation/YMLHeader.php' );
    }

    $content = YMLHeader::init();

    $args = array(
        'posts_per_page' => -1,
        'post_type'      => array('product'),
        'post_status'    => 'publish',
        'orderby'        => 'ID',
        'order'          => 'DESC'
    );
    
    if ( isset( get_option('yml_shop_settings')['categories'] ) ) {
      $args['tax_query'] = [
         [
             'taxonomy'  => 'product_cat',
             'field'     => 'term_id',
             'terms'     => get_option('yml_shop_settings')['categories']
         ]
      ];
    }

    $query = new WP_Query( $args );

    while( $query->have_posts() )
    {
      $query->the_post();
      $product = wc_get_product( $query->post->ID );

      if( !class_exists('YMLProductMetaBox') ){
        include_once( YML_PLUGIN_PATH . 'classes/product/productMetaBox.php' );
      }

      $productOptions = YMLProductMetaBox::getYML_product_options( $query->post->ID );
      $type = $productOptions['type'];
      $typeClass = 'YML' . explode( '.', $type)[0] . 'Type';

      if( !class_exists( $typeClass ) ){
        include_once( YML_PLUGIN_PATH . 'classes/generation/' . $typeClass . '.php' );
      }
      
      $content .= $typeClass::init( $product, $productOptions );
    }

		return $content;
	}
}