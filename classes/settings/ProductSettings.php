<?php

class ProductSettings
{
	
	function __construct()
	{

	}

	static function run()
	{
		add_action(
			'admin_init',
			array(
				'ProductSettings',
				'add_plugin_register_sections'
			)
		);

		add_action(
			'admin_init',
			array(
				'ProductSettings',
				'add_plugin_setting_sections'
			)
		);

		add_action(
			'admin_init',
			array(
				'ProductSettings',
				'add_plugin_setting_fields'
			)
		);
	}

	static function add_plugin_register_sections()
	{
		register_setting(
			'default_product_options',
			'yml_default_product_settings',
			array(
				'sanitize_callback' => array( 'ProductSettings', 'callback_register_my_settings')
			)
		);
	}

	static function add_plugin_setting_sections()
	{
		add_settings_section(
			'default_product_setting_section',
			'Настройки отображения продуктов',
			'',
			'default_product_setting'
		);
	}

	static function add_plugin_setting_fields()
	{
		add_settings_field(
			'slug_product_type',
			'Тип описания товара',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'type',
				'description'       => 'Выберите соответствующий тип описания продукта.',
				'type'              => 'radioBox',
				'get'               => array(
											'easy'         => 'Упрощенный тип',
											'vendor.model' => 'Произвольный тип',
											'book'         => 'Специальный тип (книги)',
											'audiobook'    => 'Специальный тип (аудиокниги)',
											'artist.title' => 'Специальный тип (Музыкальная и видеопродукция)',
											'medicine'     => 'Специальный тип (лекарства)',
											'event-ticket' => 'Специальный тип (билеты на мероприятие)',
											'tour'         => 'Специальный тип (туры)',
					),
			)
		);
		add_settings_field(
			'slug_currencyId',
			'Валюта продаже товара',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'currencyId',
				'description'       => 'Выберите валюту продажи продуктов.',
				'type'              => 'radioBox',
				'get'               => array(
											'RUR' => 'Рубль',
											'USD' => 'Доллар',
											'EUR' => 'Евро',
											'UAH' => 'Гривна',
											'KZT' => 'Тенге',
											'BYN' => 'Беларуский рубль',
										),
			)
		);
		add_settings_field(
			'slug_default_description',
			'Описание товарного предложения',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'description',
				'description'       => 'Выберите способ отображения описания товарного предложения.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'editor',
				'get'               => array(
											'none'        => 'Не отображать',
											'description' => 'Использовать текст продукта',
											'excerpt'     => 'Использовать краткое описание товара',
											'myInput'     => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_sales_notes',
			'Дополнительная информация о товарном предложении',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'sales_notes',
				'description'       => 'Выберите способ отображения дополнительной информации.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'textarea',
				'get'               => array(
											'none'      => 'Не отображать',
											'excerpt'   => 'Использовать краткое описание товара',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_vendor',
			'Производитель товара',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'vendor',
				'description'       => 'Выберите способ отображения производителя.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_model',
			'Модель товара',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'model',
				'description'       => 'Выберите способ отображения модели товара.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_vendorCode',
			'Код производитель товара',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'vendorCode',
				'description'       => 'Выберите способ отображения кода производителя.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_typePrefix',
			'Тип товарного предложения',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'typePrefix',
				'description'       => 'Выберите способ отображения типа товарного предложения.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_author',
			'Автор произведения',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'author',
				'description'       => 'Выберите способ отображения автора произведения.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_publisher',
			'Издательство произведения',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'publisher',
				'description'       => 'Выберите способ отображения издательства произведения.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_series',
			'Серия произведения',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'series',
				'description'       => 'Выберите способ отображения серии произведения.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_year',
			'Год издания произведения',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'year',
				'description'       => 'Выберите способ отображения года издания произведения.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'number',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_ISBN',
			'ISBN — международный уникальный номер книжного издания',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'ISBN',
				'description'       => 'Выберите способ отображения ISBN.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_volume',
			'Количество томов',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'volume',
				'description'       => 'Выберите способ отображения количества томов.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'number',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_part',
			'Номер тома произведения',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'part',
				'description'       => 'Выберите способ отображения номера тома произведения.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'number',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_language',
			'Язык произведения',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'language',
				'description'       => 'Выберите способ отображения языка.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_binding',
			'Переплет книги',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'binding',
				'description'       => 'Выберите способ отображения переплета.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_page_extent',
			'Количество страниц в книге',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'page_extent',
				'description'       => 'Выберите способ отображения количества страниц.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'number',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_performed_by',
			'Исполнитель аудиокниги',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'performed_by',
				'description'       => 'Выберите способ отображения исполнителя.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_storage',
			'Носитель аудиокниги',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'storage',
				'description'       => 'Выберите способ отображения носителя.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_format',
			'Формат аудиокниги',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'format',
				'description'       => 'Выберите способ отображения формата.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_recording_length',
			'Время озвучивания аудиокниги',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'recording_length',
				'description'       => 'Выберите способ отображения. Время звучания передается в формате mm.ss (минуты.секунды).',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'number',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_artist',
			'Исполнитель',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'artist',
				'description'       => 'Выберите способ отображения исполнителя.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_media',
			'Носитель',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'media',
				'description'       => 'Выберите способ отображения носителя.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_starring',
			'Актеры',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'starring',
				'description'       => 'Выберите способ отображения актеров.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_director',
			'Режиссер',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'director',
				'description'       => 'Выберите способ отображения режиссера.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_originalName',
			'Оригинальное название',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'originalName',
				'description'       => 'Выберите способ отображения оригинального названия.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_country',
			'Страна произведения(тура)',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'country',
				'description'       => 'Выберите способ отображения страны.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_param_ETK',
			'Код лекарства в Едином городском классификаторе (ЕГК)',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'param_ETK',
				'description'       => 'Выберите способ отображения кода.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_place',
			'Место проведения',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'place',
				'description'       => 'Выберите способ отображения места проведения.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_hall',
			'Зал проведения',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'hall',
				'description'       => 'Выберите способ отображения зала проведения.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_hall_part',
			'Ряд и место в зале',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'hall_part',
				'description'       => 'Выберите способ отображения ряда и места в зале.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_date',
			'Дата и время сеанса',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'date',
				'description'       => 'Выберите способ отображения даты и времени сеанса.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_worldRegion',
			'Часть света тура',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'worldRegion',
				'description'       => 'Выберите способ отображения части света.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_region',
			'Курорт или город тура',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'region',
				'description'       => 'Выберите способ отображения курорта.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_days',
			'Количество дней тура',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'days',
				'description'       => 'Выберите способ отображения количества дней.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'number',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_dataTour',
			'Дата заездов',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'dataTour',
				'description'       => 'Выберите способ отображения даты заезда.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_hotel_stars',
			'Звезды отеля',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'hotel_stars',
				'description'       => 'Выберите способ отображения звезд отеля.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'number',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_transport',
			'Транспорт отправки',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'transport',
				'description'       => 'Выберите способ отображения транспорта.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_cbid',
			'Размер ставки на карточке модели',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'cbid',
				'description'       => 'Выберите способ отображения ставки.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'number',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_bid',
			'Размер ставки на остальных местах размещения (кроме карточки модели)',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'bid',
				'description'       => 'Выберите способ отображения ставки.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'number',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_fee',
			'Размер комиссии на товарное предложение, участвующее в программе «Заказ на Маркете»',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'fee',
				'description'       => 'Выберите способ отображения ставки.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'number',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_country_of_origin',
			'Страна производитель продукции',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'country_of_origin',
				'description'       => 'Выберите способ отображения страны производителя.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'select',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'slug_default_barcode',
			'Штрихкод товара',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'barcode',
				'description'       => 'Выберите способ отображения шрихкода.',
				'type'              => 'viewsAttr',
				'typeMyInput'       => 'text',
				'get'               => array(
											'none'      => 'Не отображать',
											'attribute' => 'Использовать атрибут товара',
											'myInput'   => 'Задавать самостоятельно',
										),
			)
		);
		add_settings_field(
			'id_default_param',
			'Параметры товара',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'param',
				'description'       => 'Выберите атрибуты, которые необходимо вывести в параметры товара.',
				'type'              => 'multiselect'
			)
		);
		add_settings_field(
			'slug_default_extra_options',
			'Дополнительные параметры товара',
			 array('ProductSettings', 'callback_yml_settings_fields'),
			'default_product_setting',
			'default_product_setting_section',
			array(
				'label_for'         => 'extra',
				'description'       => 'Ваберите параметры удовлетворяющие товара.',
				'type'              => 'extraOptions',
				'get'               => array(
											'delivery'              => 'Возможность курьерской доставки товара',
											'pickup'                => 'Возможность самовывоза из пунктов выдачи',
											'store'                 => 'Возможность купить товар в розничном магазине',
											'manufacturer_warranty' => 'Товар имеет официальную гарантию производителя',
											'adult'                 => 'Товар, имеет отношение к удовлетворению сексуальных потребностей',
											'cpa'                   => 'Товар имеет возможность к дистанционной торговле',
											'weight'                => 'Указывать вес товара',
											'dimensions'            => 'Указывать габариты товара',
											'is_premiere'           => 'Премьера',
											'is_kids'               => 'Детское мероприятие'
										),
			)
		);
	}

	static function callback_yml_settings_fields( $args )
	{
		$options = get_option('yml_default_product_settings');
		if(file_exists($file_name = YML_PLUGIN_PATH . 'views/ProductSetting/SettingField/' . $args['type'] . '.php')):
			include( $file_name );
		endif;
	}

	static function callback_register_my_settings( $input )
	{
		$output = get_option('yml_default_product_settings');

		$output['type']              = $input['type'];
		$output['currencyId']        = $input['currencyId'];
		$output['vendor']            = $input['vendor'];
		$output['model']             = $input['model'];
		$output['vendorCode']        = $input['vendorCode'];
		$output['cbid']              = $input['cbid'];
		$output['bid']               = $input['bid'];
		$output['fee']               = $input['fee'];
		$output['country_of_origin'] = $input['country_of_origin'];
		$output['barcode']           = $input['barcode'];
		$output['description']       = $input['description'];
		$output['sales_notes']       = $input['sales_notes'];
		$output['param']             = $input['param'];
		$output['typePrefix']        = $input['typePrefix'];
		$output['author']            = $input['author'];
		$output['publisher']         = $input['publisher'];
		$output['series']            = $input['series'];
		$output['year']              = $input['year'];
		$output['ISBN']              = $input['ISBN'];
		$output['volume']            = $input['volume'];
		$output['part']              = $input['part'];
		$output['language']          = $input['language'];
		$output['binding']           = $input['binding'];
		$output['page_extent']       = $input['page_extent'];
		$output['performed_by']      = $input['performed_by'];
		$output['storage']           = $input['storage'];
		$output['format']            = $input['format'];
		$output['recording_length']  = $input['recording_length'];
		$output['artist']            = $input['artist'];
		$output['media']             = $input['media'];
		$output['starring']          = $input['starring'];
		$output['director']          = $input['director'];
		$output['originalName']      = $input['originalName'];
		$output['country']           = $input['country'];
		$output['param_ETK']         = $input['param_ETK'];
		$output['place']             = $input['place'];
		$output['hall']              = $input['hall'];
		$output['hall_part']         = $input['hall_part'];
		$output['date']              = $input['date'];
		$output['worldRegion']       = $input['worldRegion'];
		$output['region']            = $input['region'];
		$output['days']              = $input['days'];
		$output['dataTour']          = $input['dataTour'];
		$output['hotel_stars']       = $input['hotel_stars'];
		$output['transport']         = $input['transport'];
		
		foreach ($output['extra'] as $key => $value) {
			if( isset( $input['extra'][$key] ) ){
				$output['extra'][$key] = 'yes';
			}else{
				$output['extra'][$key] = 'off';
			}
		}

		return $output;
	}
}