<?php

class ShopSettings
{
	
	function __construct()
	{

	}

	static function run()
	{
		add_action(
			'admin_init',
			array(
				'ShopSettings',
				'add_plugin_register_sections'
			)
		);

		add_action(
			'admin_init',
			array(
				'ShopSettings',
				'add_plugin_setting_sections'
			)
		);

		add_action(
			'admin_init',
			array(
				'ShopSettings',
				'add_plugin_setting_fields'
			)
		);
	}

	static function add_plugin_register_sections()
	{
		register_setting(
			'shop_options',
			'yml_shop_settings',
			array(
				'sanitize_callback' => array( 'ShopSettings', 'callback_register_my_settings')
			)
		);
	}

	static function add_plugin_setting_sections()
	{
		add_settings_section(
			'shop_setting_section',
			'Настройка магазина',
			'',
			'shop_setting'
		);
	}

	static function add_plugin_setting_fields()
	{

		add_settings_field(
			'shop_name',
			'Название магазина',
			 array('ShopSettings', 'callback_yml_settings_fields'),
			'shop_setting',
			'shop_setting_section',
			array(
				'label_for'         => 'shop_name',
				'description'       => 'Введите название магазина. Название магазина должно совпадать с фактическим названием магазина, которое публикуется на сайте.',
				'placeholder'       => 'Название магазина',
				'type'              => 'text'
			)
		);

		add_settings_field(
			'company_name',
			'Название компании',
			 array('ShopSettings', 'callback_yml_settings_fields'),
			'shop_setting',
			'shop_setting_section',
			array(
				'label_for'         => 'company_name',
				'description'       => 'Введите название компании, владеющей магазином. Не публикуется, используется для внутренней идентификации.',
				'placeholder'       => 'Название компании',				
				'type'              => 'text'
			)
		);

		add_settings_field(
			'id_currencies',
			'Список курсов валют магазина.',
			 array('ShopSettings', 'callback_yml_settings_fields'),
			'shop_setting',
			'shop_setting_section',
			array(
				'label_for'           => 'currencies',
				'labelCurrency'       => 'currency',
				'optionCurrency'      => array(
											'RUR' => 'Рубль',
											'USD' => 'Доллар',
											'EUR' => 'Евро',
											'UAH' => 'Гривна',
											'KZT' => 'Тенге',
											'BYN' => 'Беларуский рубль',
										),
				'labelRate'           => 'rate',
				'optionRate'		  => array(
											'CBRF'   => 'Курс по Центральному банку РФ',
											'NBU'    => 'Курс по Национальному банку Украины',
											'NBK'    => 'Курс по Национальному банку Казахстана',
											'СВ'     => 'Курс по банку той страны, к которой относится магазин по своему региону',
											'MyRate' => 'Задать собственный курс',
										),
				'labelMyRate'         => 'myRate',
				'descriptionCurrency' => 'Выберите валюту, чтобы добавить.',
				'descriptionRate'     => 'Выберите банк, который устанавливает курс данной валлюты или введите свой собственный.',
				'type'                => 'currencies'
			)
		);

		add_settings_field(
			'id_delivery_options',
			'Курьерская доставка',
			 array('ShopSettings', 'callback_yml_settings_fields'),
			'shop_setting',
			'shop_setting_section',
			array(
				'label_for'         => 'deliveryShop',

				'labelCost'         => 'cost',
				'descriptionCost'   => 'Введите cтоимость курьерской доставки.',
				
				'labelDays'         => 'days',
				'descriptionDays'   => 'Введите срок курьерской доставки.',
				
				'labelOrderBefore'  => 'order-before',
				'descriptionOrder'  => 'Введите время оформления заказа, до которого действуют указанные условия доставки.',
				
				'type'              => 'deliveryShop'
			)
		);

		add_settings_field(
			'id_categories',
			'Категории',
			 array('ShopSettings', 'callback_yml_settings_fields'),
			'shop_setting',
			'shop_setting_section',
			array(
				'label_for'         => 'categories',
				'description'       => 'Выберите категории, которые необходимо вывести.',
				'type'              => 'multiselect'
			)
		);
	}

	static function callback_yml_settings_fields( $args )
	{
		$options = get_option('yml_shop_settings');

		if(file_exists($file_name = YML_PLUGIN_PATH . 'views/ShopSetting/SettingField/' . $args['type'] . '.php')):
			include( $file_name );
		endif;
	}

	function callback_register_my_settings( $input )
	{
		$output = get_option('yml_shop_settings');

		$output['shop_name']    = sanitize_text_field($input['shop_name']);
		$output['company_name'] = sanitize_text_field($input['company_name']);
		$output['currencies']   = $input['currencies'];
		$output['deliveryShop'] = $input['deliveryShop'];
		$output['categories']	= $input['categories'];

		return $output;
	}
}