<?php

/**
* 
*/
class ShopPage
{
	
	function __construct()
	{

	}

	static function run()
	{
		add_action(
			'admin_menu',
			array(
				'ShopPage',
				'admin_menu_pages'
			)
		);
		if( !class_exists( 'ShopSettings' ) ):
			include_once( YML_PLUGIN_PATH . 'classes/settings/ShopSettings.php' );
		endif;

		ShopSettings::run();
	}

	static function admin_menu_pages()
	{
		add_menu_page(
			'Меню настроек YML Generation плагина',
			'Генератор yml-файла',
			'manage_options',
			'shop-setting',
			 array( 'ShopPage', 'getShopSetting' ),
			'dashicons-analytics',
			54.9999
		);

/*		add_submenu_page(
			'shop-setting',
			'Меню настроек магазина',
			'Настройка магазина',
			'manage_options',
			'product-setting',
			 array( 'ShopPage', 'getProductSetting' )
		);

		add_submenu_page(
			'yml-generation/views/AdminView.php',
			'Меню настроек товаров',
			'Настройка товаров',
			'manage_options',
			'yml-generation/views/page3.php',
			''
		);*/
	}

	static function getShopSetting(){
		if( file_exists( YML_PLUGIN_PATH . 'views/ShopSetting/ShopSetting.php' ) ):
			include_once(YML_PLUGIN_PATH . 'views/ShopSetting/ShopSetting.php');
		endif;
	}
}