<?php

/**
* 
*/
class GenerationPage
{
	
	function __construct()
	{

	}

	static function run()
	{
		add_action(
			'admin_menu',
			array(
				'GenerationPage',
				'admin_menu_pages'
			)
		);
		/*if( !class_exists( 'ProductSettings' ) ):
			include_once( YML_PLUGIN_PATH . 'classes/settings/ProductSettings.php' );
		endif;

		ProductSettings::run();*/
	}

	static function admin_menu_pages()
	{
		add_submenu_page(
			'shop-setting',
			'Генерирование файла',
			'Генерирование файла',
			'manage_options',
			'generate',
			 array( 'GenerationPage', 'getGenerationPage' )
		);
	}

	static function getGenerationPage(){
		if( file_exists( YML_PLUGIN_PATH . 'views/GenerationPage/generationVeiw.php' ) ):
			include_once(YML_PLUGIN_PATH . 'views/GenerationPage/generationVeiw.php');
		endif;
	}
}