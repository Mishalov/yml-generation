<?php

/**
* 
*/
class ProductPage
{
	
	function __construct()
	{

	}

	static function run()
	{
		add_action(
			'admin_menu',
			array(
				'ProductPage',
				'admin_menu_pages'
			)
		);
		if( !class_exists( 'ProductSettings' ) ):
			include_once( YML_PLUGIN_PATH . 'classes/settings/ProductSettings.php' );
		endif;

		ProductSettings::run();
	}

	static function admin_menu_pages()
	{
		add_submenu_page(
			'shop-setting',
			'Меню настроек магазина',
			'Настройка магазина',
			'manage_options',
			'product-setting',
			 array( 'ProductPage', 'getProductSetting' )
		);
	}

	static function getProductSetting(){
		if( file_exists( YML_PLUGIN_PATH . 'views/ProductSetting/ProductSetting.php' ) ):
			include_once(YML_PLUGIN_PATH . 'views/ProductSetting/ProductSetting.php');
		endif;
	}
}