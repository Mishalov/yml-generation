var all = jQuery('select[multiple] option').size();
jQuery('select[multiple]').attr('size', all);

function onChangeInput(object, data)
{
	var obj_attribute = 'p[data-lable="'+data+'-attribute"]';
	var obj_myInput = 'p[data-lable="'+data+'-myInput"]';
	if(jQuery(object).val() == 'attribute'){
		jQuery(obj_myInput).fadeOut(
				function(){
					jQuery(obj_attribute).fadeIn();
				}
			);
	}else if(jQuery(object).val() == 'myInput'){
		jQuery(obj_attribute).fadeOut(
				function(){
					jQuery(obj_myInput).fadeIn();
				}
			);
	}else{
		jQuery(obj_attribute).fadeOut();
		jQuery(obj_myInput).fadeOut();
	}
}

var allOptions = [
						'currencyId',
						'vendor',
						'model',
						'vendorCode',
						'cbid',
						'bid',
						'fee',
						'country_of_origin',
						'barcode',
						'description',
						'sales_notes',
						'param',
						'expiry',
						'typePrefix',
						'author',
						'publisher',
						'series',
						'year',
						'ISBN',
						'volume',
						'part',
						'language',
						'binding',
						'page_extent',
						'performed_by',
						'storage',
						'format',
						'recording_length',
						'artist',
						'media',
						'starring',
						'director',
						'originalName',
						'country',
						'param_ETK',
						'place',
						'hall',
						'hall_part',
						'date',
						'worldRegion',
						'region',
						'days',
						'dataTour',
						'hotel_stars',
						'transport',
					 ];
var easyOptions = [
						'currencyId',
						'vendor',
						'model',
						'vendorCode',
						'cbid',
						'bid',
						'fee',
						'country_of_origin',
						'barcode',
						'description',
						'sales_notes',
						'param',
						'expiry',
					 ];
var vendorOptions = [
						'currencyId',
						'vendor',
						'model',
						'vendorCode',
						'cbid',
						'bid',
						'fee',
						'country_of_origin',
						'barcode',
						'description',
						'sales_notes',
						'param',
						'expiry',
						'typePrefix',
						];
var bookOptions =  [
						'currencyId',
						'cbid',
						'bid',
						'fee',
						'country_of_origin',
						'barcode',
						'description',
						'sales_notes',
						'param',
						'author',
						'publisher',
						'series',
						'year',
						'ISBN',
						'volume',
						'part',
						'language',
						'binding',
						'page_extent',
					 ];
var audiobookOptions = [
						'currencyId',
						'cbid',
						'bid',
						'fee',
						'country_of_origin',
						'barcode',
						'description',
						'sales_notes',
						'param',
						'author',
						'publisher',
						'series',
						'year',
						'ISBN',
						'volume',
						'part',
						'language',
						'performed_by',
						'format',
						'recording_length',
					 ];
var artistOptions = [
						'currencyId',
						'cbid',
						'bid',
						'fee',
						'country_of_origin',
						'barcode',
						'description',
						'sales_notes',
						'param',
						'year',
						'artist',
						'media',
						'starring',
						'director',
						'originalName',
						'country',
					 ];
var medicineOptions = [
						'currencyId',
						'vendor',
						'vendorCode',
						'cbid',
						'bid',
						'fee',
						'country_of_origin',
						'barcode',
						'description',
						'sales_notes',
						'param',
						'expiry',
						'param_ETK',
					 ];
var eventOptions = [
						'currencyId',
						'cbid',
						'bid',
						'fee',
						'country_of_origin',
						'barcode',
						'description',
						'sales_notes',
						'place',
						'hall',
						'hall_part',
						'date',
					 ];
var tourOptions = [
						'currencyId',
						'cbid',
						'bid',
						'fee',
						'country_of_origin',
						'barcode',
						'description',
						'sales_notes',
						'param',
						'worldRegion',
						'region',
						'days',
						'dataTour',
						'hotel_stars',
						'transport',
					 ];

jQuery(document).ready(function($) {

	var type = $('#product_default_type input:radio:checked').val();

	switch( type ) {
		case 'easy':
			for (var i = 0; i < allOptions.length; i++) {
				if( easyOptions.indexOf(allOptions[i]) < 0 ){
					$('#product_default_' + allOptions[i] ).parent().parent().css('display', 'none');
				}
			}
			break;
		case 'vendor.model':
			for (var i = 0; i < allOptions.length; i++) {
				if( vendorOptions.indexOf(allOptions[i]) < 0 ){
					$('#product_default_' + allOptions[i] ).parent().parent().css('display', 'none');
				}
			}
			break;
		case 'book':
			for (var i = 0; i < allOptions.length; i++) {
				if( bookOptions.indexOf(allOptions[i]) < 0 ){
					$('#product_default_' + allOptions[i] ).parent().parent().css('display', 'none');
				}
			}
			break;
		case 'audiobook':
			for (var i = 0; i < allOptions.length; i++) {
				if( audiobookOptions.indexOf(allOptions[i]) < 0 ){
					$('#product_default_' + allOptions[i] ).parent().parent().css('display', 'none');
				}
			}
			break;
		case 'artist.title':
			for (var i = 0; i < allOptions.length; i++) {
				if( artistOptions.indexOf(allOptions[i]) < 0 ){
					$('#product_default_' + allOptions[i] ).parent().parent().css('display', 'none');
				}
			}
			break;
		case 'medicine':
			for (var i = 0; i < allOptions.length; i++) {
				if( medicineOptions.indexOf(allOptions[i]) < 0 ){
					$('#product_default_' + allOptions[i] ).parent().parent().css('display', 'none');
				}
			}
			break;
		case 'event-ticket':
			for (var i = 0; i < allOptions.length; i++) {
				if( eventOptions.indexOf(allOptions[i]) < 0 ){
					$('#product_default_' + allOptions[i] ).parent().parent().css('display', 'none');
				}
			}
			break;
		case 'tour':
			for (var i = 0; i < allOptions.length; i++) {
				if( tourOptions.indexOf(allOptions[i]) < 0 ){
					$('#product_default_' + allOptions[i] ).parent().parent().css('display', 'none');
				}
			}
			break;
		default:
			break;
	}

});

function addProductType(object)
{
	switch( object.value ) {
		case 'easy':
			for (var i = 0; i < allOptions.length; i++) {
				if( easyOptions.indexOf(allOptions[i]) < 0 ){
					jQuery('#product_default_' + allOptions[i] ).parent().parent().fadeOut();
				}else{
					jQuery('#product_default_' + allOptions[i] ).parent().parent().fadeIn();
				}
			}
			break;
		case 'vendor.model':
			for (var i = 0; i < allOptions.length; i++) {
				if( vendorOptions.indexOf(allOptions[i]) < 0 ){
					jQuery('#product_default_' + allOptions[i] ).parent().parent().fadeOut();
				}else{
					jQuery('#product_default_' + allOptions[i] ).parent().parent().fadeIn();
				}
			}
			break;
		case 'book':
			for (var i = 0; i < allOptions.length; i++) {
				if( bookOptions.indexOf(allOptions[i]) < 0 ){
					jQuery('#product_default_' + allOptions[i] ).parent().parent().fadeOut();
				}else{
					jQuery('#product_default_' + allOptions[i] ).parent().parent().fadeIn();
				}
			}
			break;
		case 'audiobook':
			for (var i = 0; i < allOptions.length; i++) {
				if( audiobookOptions.indexOf(allOptions[i]) < 0 ){
					jQuery('#product_default_' + allOptions[i] ).parent().parent().fadeOut();
				}else{
					jQuery('#product_default_' + allOptions[i] ).parent().parent().fadeIn();
				}
			}
			break;
		case 'artist.title':
			for (var i = 0; i < allOptions.length; i++) {
				if( artistOptions.indexOf(allOptions[i]) < 0 ){
					jQuery('#product_default_' + allOptions[i] ).parent().parent().fadeOut();
				}else{
					jQuery('#product_default_' + allOptions[i] ).parent().parent().fadeIn();
				}
			}
			break;
		case 'medicine':
			for (var i = 0; i < allOptions.length; i++) {
				if( medicineOptions.indexOf(allOptions[i]) < 0 ){
					jQuery('#product_default_' + allOptions[i] ).parent().parent().fadeOut();
				}else{
					jQuery('#product_default_' + allOptions[i] ).parent().parent().fadeIn();
				}
			}
			break;
		case 'event-ticket':
			for (var i = 0; i < allOptions.length; i++) {
				if( eventOptions.indexOf(allOptions[i]) < 0 ){
					jQuery('#product_default_' + allOptions[i] ).parent().parent().fadeOut();
				}else{
					jQuery('#product_default_' + allOptions[i] ).parent().parent().fadeIn();
				}
			}
			break;
		case 'tour':
			for (var i = 0; i < allOptions.length; i++) {
				if( tourOptions.indexOf(allOptions[i]) < 0 ){
					jQuery('#product_default_' + allOptions[i] ).parent().parent().fadeOut();
				}else{
					jQuery('#product_default_' + allOptions[i] ).parent().parent().fadeIn();
				}
			}
			break;
		default:
			break;
	}
}