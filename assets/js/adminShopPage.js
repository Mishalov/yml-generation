var all = jQuery('select[multiple] option').size();
jQuery('select[multiple]').attr('size', all);

function deleteDeliverySection( numDelivery )
{
	jQuery('#allDeliveryShop .deliveryShop[data-num="' + numDelivery + '"]').fadeOut(
		function(){
			jQuery('#allDeliveryShop .deliveryShop[data-num="' + numDelivery + '"]').remove();
		});
}

function addDeliverySection(label_for, labelCost, descriptionCost, labelDays, descriptionDays, labelOrderBefore, descriptionOrder)
{
	var newNumDelivery = 0;
	var last_number = parseInt(jQuery('#allDeliveryShop .deliveryShop').last().attr('data-num'));

	if(!isNaN(last_number)){
		newNumDelivery += last_number + 1;
	}

	var newDeliverySection = 
						'<section class="deliveryShop" data-num="' + newNumDelivery + '" style="display: none">' +
							'<div class="inputDelivery">' +
								'<input type="number" class="deliveryInput" name="yml_shop_settings[' +
										label_for + '][' + newNumDelivery + '][' + labelCost + ']" min="0" value="0">' +
								'<p class="description">' + descriptionCost + '</p>' +
							'</div>' +
							'<div class="inputDelivery">' +
								'<input type="number" class="deliveryInput" name="yml_shop_settings[' +
										label_for + '][' + newNumDelivery + '][' + labelDays +
										']" min="0" max="32" value="0">' +
								'<p class="description">' + descriptionDays + '</p>' +
							'</div>' +
							'<div class="inputDelivery">' +
								'<input type="number" class="deliveryInput" name="yml_shop_settings[' +
									label_for + '][' + newNumDelivery + '][' + labelOrderBefore +
									']" min="0" max="24" value="0">' +
								'<p class="description">' + descriptionOrder + '</p>' +
							'</div>' +
							'<button class="plusDelivery" type="button" onClick="deleteDeliverySection(' + newNumDelivery + ');' +
										'return false;" style="clear: none;">X</button>' +
							'</section>';

	jQuery('#allDeliveryShop').append(newDeliverySection);
	jQuery('#allDeliveryShop .deliveryShop').last().fadeIn();
}

function addMyRate( numCurresy, label_for, labelRate )
{
	var selectedRate = jQuery('#' + label_for + labelRate + numCurresy );
	if( selectedRate.val() == 'MyRate' ){
		selectedRate.next().fadeIn();
	}
	else{
		selectedRate.next().fadeOut();
	}
}

function deleteCurrencySection( numCurresy )
{
	jQuery('#allCurrencies div.allCurrencies[data-num="' + numCurresy + '"]').fadeOut(
		function(){
			jQuery('#allCurrencies div.allCurrencies[data-num="' + numCurresy + '"]').remove();
		});
}

function addCurrencySection(label_for, labelCurrency, numCurresy, labelMyRate, descriptionRate, labelRate, descriptionCurrency)
{
	var newNumCurrency = 0;
	var last_number = parseInt(jQuery('#allCurrencies div.allCurrencies').last().attr('data-num'));

	if(!isNaN(last_number)){
		newNumCurrency += last_number + 1;
	}

	var newCurrencySection = '<div class="allCurrencies" data-num="' + newNumCurrency + '" style="display: none">' +
			'<section class="currency">' +
			'<select id="' + label_for + labelCurrency + newNumCurrency + '"' +
					'name="yml_shop_settings[' + label_for + '][' + newNumCurrency + '][' + labelCurrency + ']">' +

					'<option value="RUR">Рубль</option>' +
					'<option value="USD">Доллар</option>' +
					'<option value="EUR" >Евро</option>' +
					'<option value="UAH" >Гривна</option>' +
					'<option value="KZT" >Тенге</option>' +
					'<option value="BYN" >Беларуский рубль</option>' +

			'</select>' +

			'<p class="descriptionCurrency">' +
				descriptionCurrency +
			'</p>' +

		'</section>' +
		'<section class="Rate" style="margin: 0;">' +
			'<select id="' + label_for + labelRate + newNumCurrency + '"' +
					'name="yml_shop_settings[' + label_for + '][' + newNumCurrency + '][' + labelRate + ']"' +
					'style="max-width: 70%;" onChange="addMyRate(' + numCurresy + ',' +
						'\'' + label_for + '\', \'' + labelRate + '\');">' +

					'<option value="CBRF" >Курс по Центральному банку РФ</option>' +
					'<option value="NBU" >Курс по Национальному банку Украины</option>' +
					'<option value="NBK" >Курс по Национальному банку Казахстана</option>' +
					'<option value="СВ" >Курс по банку той страны, к которой относится магазин по своему региону</option>' +
					'<option value="MyRate">Задать собственный курс</option>' +

			'</select>' +

			'<input type="number" name="yml_shop_settings[' + label_for + '][' + newNumCurrency + '][' + labelMyRate + ']" min="0" style="display: none; width: 28%;" data-num="' +
			newNumCurrency + '">' +

			'<p class="descriptionRate">' +
				descriptionRate +
			'</p>' +
		'</section>' +
	'<button class="plusCurrency" onClick="deleteCurrencySection(' + newNumCurrency +
	'); return false;" style="clear: none;">X</button>' +
	'</div>';

		jQuery('#allCurrencies').append(newCurrencySection);
		jQuery('#allCurrencies div.allCurrencies').last().fadeIn();
}