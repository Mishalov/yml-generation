var allOptions = [
						'currencyId',
						'vendor',
						'model',
						'vendorCode',
						'cbid',
						'bid',
						'fee',
						'country_of_origin',
						'barcode',
						'description',
						'sales_notes',
						'param',
						'expiry',
						'typePrefix',
						'author',
						'publisher',
						'series',
						'year',
						'ISBN',
						'volume',
						'part',
						'language',
						'binding',
						'page_extent',
						'performed_by',
						'storage',
						'format',
						'recording_length',
						'artist',
						'media',
						'starring',
						'director',
						'originalName',
						'country',
						'ETK',
						'place',
						'hall',
						'hall_part',
						'date',
						'worldRegion',
						'region',
						'days',
						'dataTour',
						'hotel_stars',
						'transport',
						'delivery',
						'pickup',
						'store',
						'cpa',
						'is_kids',
						'adult',
						'is_premiere',
						'demensions',
						'weight',
						'manufacturer_warranty',
					 ];
var easyOptions = [
						'currencyId',
						'vendor',
						'model',
						'vendorCode',
						'cbid',
						'bid',
						'fee',
						'country_of_origin',
						'barcode',
						'description',
						'sales_notes',
						'param',
						'expiry',
						'delivery',
						'pickup',
						'store',
						'cpa',
						'adult',
						'demensions',
						'weight',
						'manufacturer_warranty',
					 ];
var vendorOptions = [
						'currencyId',
						'vendor',
						'model',
						'vendorCode',
						'cbid',
						'bid',
						'fee',
						'country_of_origin',
						'barcode',
						'description',
						'sales_notes',
						'param',
						'expiry',
						'typePrefix',
						'delivery',
						'pickup',
						'store',
						'cpa',
						'adult',
						'demensions',
						'weight',
						'manufacturer_warranty',
						];
var bookOptions =  [
						'currencyId',
						'cbid',
						'bid',
						'fee',
						'country_of_origin',
						'barcode',
						'description',
						'sales_notes',
						'param',
						'author',
						'publisher',
						'series',
						'year',
						'ISBN',
						'volume',
						'part',
						'language',
						'binding',
						'page_extent',
						'delivery',
						'pickup',
						'store',
						'cpa',
						'adult',
						'demensions',
						'weight',
						'manufacturer_warranty',
					 ];
var audiobookOptions = [
						'currencyId',
						'cbid',
						'bid',
						'fee',
						'country_of_origin',
						'barcode',
						'description',
						'sales_notes',
						'param',
						'author',
						'publisher',
						'series',
						'year',
						'ISBN',
						'volume',
						'part',
						'language',
						'performed_by',
						'format',
						'recording_length',
						'pickup',
						'store',
						'cpa',
						'is_kids',
						'adult',
						'demensions',
						'weight',
						'manufacturer_warranty',
					 ];
var artistOptions = [
						'currencyId',
						'cbid',
						'bid',
						'fee',
						'country_of_origin',
						'barcode',
						'description',
						'sales_notes',
						'param',
						'year',
						'artist',
						'media',
						'starring',
						'director',
						'originalName',
						'country',
						'delivery',
						'pickup',
						'store',
						'cpa',
						'is_kids',
						'adult',
						'is_premiere',
						'demensions',
						'manufacturer_warranty',
					 ];
var medicineOptions = [
						'currencyId',
						'vendor',
						'vendorCode',
						'cbid',
						'bid',
						'fee',
						'country_of_origin',
						'barcode',
						'description',
						'sales_notes',
						'param',
						'expiry',
						'ETK',
						'delivery',
						'pickup',
						'store',
						'cpa',
						'is_kids',
						'adult',
						'is_premiere',
						'demensions',
						'weight',
						'manufacturer_warranty',
					 ];
var eventOptions = [
						'currencyId',
						'cbid',
						'bid',
						'fee',
						'country_of_origin',
						'barcode',
						'description',
						'sales_notes',
						'place',
						'hall',
						'hall_part',
						'date',
						'delivery',
						'pickup',
						'store',
						'cpa',
						'is_kids',
						'adult',
						'is_premiere',
						'demensions',
						'weight',
						'manufacturer_warranty',
					 ];
var tourOptions = [
						'currencyId',
						'cbid',
						'bid',
						'fee',
						'country_of_origin',
						'barcode',
						'description',
						'sales_notes',
						'param',
						'worldRegion',
						'region',
						'days',
						'dataTour',
						'hotel_stars',
						'transport',
						'delivery',
						'pickup',
						'store',
						'cpa',
						'is_kids',
						'adult',
						'is_premiere',
						'demensions',
						'weight',
						'manufacturer_warranty',
					 ];

jQuery('document').ready(function($){

	$('div#wp-_yml_description_myInput-wrap').css('width', '90%').css('margin', '0 5%');

	allOptions.forEach(function(item, i, arr) {

		var value = $('select#_yml_' + item).val();
		var options = 'p.default_mis_field';

		$('select#_yml_' + item + ' option').each(function(){
			options += ', p.form-field._yml_' + item + '_' + this.value + '_field';
		});

		$(options).css('display', 'none');
		$('p.form-field._yml_' + item + '_' + value + '_field').css('display', 'block');

	});

	if( $('select#_yml_description').val() == 'myInput' ){
		$('div#wp-_yml_description_myInput-wrap').css('display', 'block');
	}else{
		$('div#wp-_yml_description_myInput-wrap').css('display', 'none');
	}

	var type = $('select#_yml_type').val();

	switch( type ) {
		case 'easy':
			for (var i = 0; i < allOptions.length; i++) {
				if( easyOptions.indexOf(allOptions[i]) < 0 ){
					var options = 'p.form-field._yml_' + allOptions[i] + '_field';

					$('select#_yml_' + allOptions[i] + ' option').each(function(){
						options += ', p.form-field._yml_' + allOptions[i] + '_' + this.value + '_field';
					});
					$(options).css('display', 'none');
				}
			}
			break;
		case 'vendor.model':
			for (var i = 0; i < allOptions.length; i++) {
				if( vendorOptions.indexOf(allOptions[i]) < 0 ){
					var options = 'p.form-field._yml_' + allOptions[i] + '_field';

					$('select#_yml_' + allOptions[i] + ' option').each(function(){
						options += ', p.form-field._yml_' + allOptions[i] + '_' + this.value + '_field';
					});
					$(options).css('display', 'none');
				}
			}
			break;
		case 'book':
			for (var i = 0; i < allOptions.length; i++) {
				if( bookOptions.indexOf(allOptions[i]) < 0 ){
					var options = 'p.form-field._yml_' + allOptions[i] + '_field';

					$('select#_yml_' + allOptions[i] + ' option').each(function(){
						options += ', p.form-field._yml_' + allOptions[i] + '_' + this.value + '_field';
					});
					$(options).css('display', 'none');
				}
			}
			break;
		case 'audiobook':
			for (var i = 0; i < allOptions.length; i++) {
				if( audiobookOptions.indexOf(allOptions[i]) < 0 ){
					var options = 'p.form-field._yml_' + allOptions[i] + '_field';

					$('select#_yml_' + allOptions[i] + ' option').each(function(){
						options += ', p.form-field._yml_' + allOptions[i] + '_' + this.value + '_field';
					});
					$(options).css('display', 'none');
				}
			}
			break;
		case 'artist.title':
			for (var i = 0; i < allOptions.length; i++) {
				if( artistOptions.indexOf(allOptions[i]) < 0 ){
					var options = 'p.form-field._yml_' + allOptions[i] + '_field';

					$('select#_yml_' + allOptions[i] + ' option').each(function(){
						options += ', p.form-field._yml_' + allOptions[i] + '_' + this.value + '_field';
					});
					$(options).css('display', 'none');
				}
			}
			break;
		case 'medicine':
			for (var i = 0; i < allOptions.length; i++) {
				if( medicineOptions.indexOf(allOptions[i]) < 0 ){
					var options = 'p.form-field._yml_' + allOptions[i] + '_field';

					$('select#_yml_' + allOptions[i] + ' option').each(function(){
						options += ', p.form-field._yml_' + allOptions[i] + '_' + this.value + '_field';
					});
					$(options).css('display', 'none');
				}
			}
			break;
		case 'event-ticket':
			for (var i = 0; i < allOptions.length; i++) {
				if( eventOptions.indexOf(allOptions[i]) < 0 ){
					var options = 'p.form-field._yml_' + allOptions[i] + '_field';

					$('select#_yml_' + allOptions[i] + ' option').each(function(){
						options += ', p.form-field._yml_' + allOptions[i] + '_' + this.value + '_field';
					});
					$(options).css('display', 'none');
				}
			}
			break;
		case 'tour':
			for (var i = 0; i < allOptions.length; i++) {
				if( tourOptions.indexOf(allOptions[i]) < 0 ){
					var options = 'p.form-field._yml_' + allOptions[i] + '_field';

					$('select#_yml_' + allOptions[i] + ' option').each(function(){
						options += ', p.form-field._yml_' + allOptions[i] + '_' + this.value + '_field';
					});
					$(options).css('display', 'none');
				}
			}
			break;
		default:
			break;
	}
});

function mis_views_yml_option(object)
	{
		var attr = jQuery(object).attr('data-lable');
		var value = object.value;
		var objOut = 'p.default_mis_field';

		jQuery('select#_yml_' + attr + ' option').each(function(){
			objOut += ', p.form-field._yml_' + attr + '_' + this.value + '_field';
		});
		jQuery(objOut).fadeOut(function(){
			jQuery('p.form-field._yml_' + attr + '_' + value + '_field').fadeIn();
		});
	}

function mis_views_yml_description(object)
	{
		if(object.value == 'myInput'){
			jQuery('div#wp-_yml_description_myInput-wrap').fadeIn();
		}else{
			jQuery('div#wp-_yml_description_myInput-wrap').fadeOut();
		}
	}



function mis_type_yml_option(type)
{
	switch( type ) {
		case 'easy':
			for (var i = 0; i < allOptions.length; i++) {
				if( easyOptions.indexOf(allOptions[i]) < 0 ){
					var options = 'p.form-field._yml_' + allOptions[i] + '_field';

					jQuery('select#_yml_' + allOptions[i] + ' option').each(function(){
						options += ', p.form-field._yml_' + allOptions[i] + '_' + this.value + '_field';
					});
					jQuery(options).fadeOut();
				}else{
					var options = 'p.form-field._yml_' + allOptions[i] + '_field, p.form-field._yml_' + allOptions[i] + '_' + jQuery('select#_yml_' + allOptions[i] + ' option').val() + '_field';

					jQuery(options).fadeIn();
				}
			}
			break;
		case 'vendor.model':
			for (var i = 0; i < allOptions.length; i++) {
				if( vendorOptions.indexOf(allOptions[i]) < 0 ){
					var options = 'p.form-field._yml_' + allOptions[i] + '_field';

					jQuery('select#_yml_' + allOptions[i] + ' option').each(function(){
						options += ', p.form-field._yml_' + allOptions[i] + '_' + this.value + '_field';
					});
					jQuery(options).fadeOut();
				}else{
					var options = 'p.form-field._yml_' + allOptions[i] + '_field, p.form-field._yml_' + allOptions[i] + '_' + jQuery('select#_yml_' + allOptions[i] + ' option').val() + '_field';

					jQuery(options).fadeIn();
				}
			}
			break;
		case 'book':
			for (var i = 0; i < allOptions.length; i++) {
				if( bookOptions.indexOf(allOptions[i]) < 0 ){
					var options = 'p.form-field._yml_' + allOptions[i] + '_field';

					jQuery('select#_yml_' + allOptions[i] + ' option').each(function(){
						options += ', p.form-field._yml_' + allOptions[i] + '_' + this.value + '_field';
					});
					jQuery(options).fadeOut();
				}else{
					var options = 'p.form-field._yml_' + allOptions[i] + '_field, p.form-field._yml_' + allOptions[i] + '_' + jQuery('select#_yml_' + allOptions[i] + ' option').val() + '_field';

					jQuery(options).fadeIn();
				}
			}
			break;
		case 'audiobook':
			for (var i = 0; i < allOptions.length; i++) {
				if( audiobookOptions.indexOf(allOptions[i]) < 0 ){
					var options = 'p.form-field._yml_' + allOptions[i] + '_field';

					jQuery('select#_yml_' + allOptions[i] + ' option').each(function(){
						options += ', p.form-field._yml_' + allOptions[i] + '_' + this.value + '_field';
					});
					jQuery(options).fadeOut();
				}else{
					var options = 'p.form-field._yml_' + allOptions[i] + '_field, p.form-field._yml_' + allOptions[i] + '_' + jQuery('select#_yml_' + allOptions[i] + ' option').val() + '_field';

					jQuery(options).fadeIn();
				}
			}
			break;
		case 'artist.title':
			for (var i = 0; i < allOptions.length; i++) {
				if( artistOptions.indexOf(allOptions[i]) < 0 ){
					var options = 'p.form-field._yml_' + allOptions[i] + '_field';

					jQuery('select#_yml_' + allOptions[i] + ' option').each(function(){
						options += ', p.form-field._yml_' + allOptions[i] + '_' + this.value + '_field';
					});
					jQuery(options).fadeOut();
				}else{
					var options = 'p.form-field._yml_' + allOptions[i] + '_field, p.form-field._yml_' + allOptions[i] + '_' + jQuery('select#_yml_' + allOptions[i] + ' option').val() + '_field';

					jQuery(options).fadeIn();
				}
			}
			break;
		case 'medicine':
			for (var i = 0; i < allOptions.length; i++) {
				if( medicineOptions.indexOf(allOptions[i]) < 0 ){
					var options = 'p.form-field._yml_' + allOptions[i] + '_field';

					jQuery('select#_yml_' + allOptions[i] + ' option').each(function(){
						options += ', p.form-field._yml_' + allOptions[i] + '_' + this.value + '_field';
					});
					jQuery(options).fadeOut();
				}else{
					var options = 'p.form-field._yml_' + allOptions[i] + '_field, p.form-field._yml_' + allOptions[i] + '_' + jQuery('select#_yml_' + allOptions[i] + ' option').val() + '_field';

					jQuery(options).fadeIn();
				}
			}
			break;
		case 'event-ticket':
			for (var i = 0; i < allOptions.length; i++) {
				if( eventOptions.indexOf(allOptions[i]) < 0 ){
					var options = 'p.form-field._yml_' + allOptions[i] + '_field';

					jQuery('select#_yml_' + allOptions[i] + ' option').each(function(){
						options += ', p.form-field._yml_' + allOptions[i] + '_' + this.value + '_field';
					});
					jQuery(options).fadeOut();
				}else{
					var options = 'p.form-field._yml_' + allOptions[i] + '_field, p.form-field._yml_' + allOptions[i] + '_' + jQuery('select#_yml_' + allOptions[i] + ' option').val() + '_field';

					jQuery(options).fadeIn();
				}
			}
			break;
		case 'tour':
			for (var i = 0; i < allOptions.length; i++) {
				if( tourOptions.indexOf(allOptions[i]) < 0 ){
					var options = 'p.form-field._yml_' + allOptions[i] + '_field';

					jQuery('select#_yml_' + allOptions[i] + ' option').each(function(){
						options += ', p.form-field._yml_' + allOptions[i] + '_' + this.value + '_field';
					});
					jQuery(options).fadeOut();
				}else{
					var options = 'p.form-field._yml_' + allOptions[i] + '_field, p.form-field._yml_' + allOptions[i] + '_' + jQuery('select#_yml_' + allOptions[i] + ' option').val() + '_field';

					jQuery(options).fadeIn();
				}
			}
			break;
		default:
			break;
	}
}