<?php

add_action('wp_ajax_add_yml_generation_file', 'add_yml_generation_file_callback');

function add_yml_generation_file_callback()
{
  if (empty($wp_filesystem)) {
      require_once (ABSPATH . '/wp-admin/includes/file.php');
      WP_Filesystem();
      global $wp_filesystem;
  }

    // Get the upload directory and make a ym-export-YYYY-mm-dd.yml file.
    $upload_dir = wp_upload_dir();
    $folder     = trailingslashit( $upload_dir['basedir'] ) . trailingslashit( 'yml-files' );
    $filename   = 'yml-' . date( "Y-m-d_G:i:s" ) . '.txt';
    $lastfilename = '_yml-last-file.txt';

    $filepath = $folder . $filename;
    $lastfilepath = $folder . $lastfilename;

    // Check if 'uploads/market-exporter' folder exists. If not - create it.
    if ( ! $wp_filesystem->exists( $folder ) ) {
      if ( ! $wp_filesystem->mkdir( $folder, FS_CHMOD_DIR ) ) {
        _e( "Error creating directory.", 'market-exporter' );
      }
    }

    if( !class_exists('YMLGenerationFile') ){
      include_once( YML_PLUGIN_PATH . 'classes/generation/YMLGenerationFile.php' );
    }

    $content = YMLGenerationFile::init();

    // Create the file.
    if ( !$wp_filesystem->put_contents( $filepath, $content, FS_CHMOD_FILE ) ) {
      _e( "Error uploading file.", 'market-exporter' );
    }
    if ( !$wp_filesystem->put_contents( $lastfilepath, $content, FS_CHMOD_FILE ) ) {
      _e( "Error uploading file.", 'market-exporter' );
    }

  $dir = wp_upload_dir()['basedir'] . '/yml-files/';
  $files = $wp_filesystem->dirlist( $dir );

  $answer = '';

  if ( $files ):
    foreach( $files as $file ):
      $answer .= '<tr>'.PHP_EOL;
      $answer .= '  <td class="row-title"><input type="checkbox" name="files[]" value="' . $file['name'] . '"></td>'.PHP_EOL;
      $answer .= '  <td>' . $file['name'] . '</td>'.PHP_EOL;
      $answer .= '  <td><a href="' . wp_upload_dir()['baseurl'] . '/yml-files/' . $file['name'] . '" target="_blank">Просмотреть файл</a></td>'.PHP_EOL;
      $answer .= '</tr>'.PHP_EOL;
    endforeach;
  endif;

  echo $answer;

  wp_die();
}


add_action('wp_ajax_remove_yml_generation_files', 'remove_yml_generation_files_callback');

function remove_yml_generation_files_callback()
{
  if (empty($wp_filesystem)) {
      require_once (ABSPATH . '/wp-admin/includes/file.php');
      WP_Filesystem();
      global $wp_filesystem;
  }

    // Get the upload directory and make a ym-export-YYYY-mm-dd.yml file.
    $upload_dir = wp_upload_dir();
    $folder     = trailingslashit( $upload_dir['basedir'] ) . trailingslashit( 'yml-files' );
    $filename   = 'yml-' . date( "Y-m-d_G:i:s" ) . '.yml';
    $lastfilename = '_yml-last-file.yml';

    $filepath = $folder . $filename;
    $lastfilepath = $folder . $lastfilename;

  $yml_files = json_decode( json_encode( $_POST['yml_files'] ) );

  foreach($yml_files as $file)
  {
    $wp_filesystem->delete( $folder . $file );
  }  

  $dir = wp_upload_dir()['basedir'] . '/yml-files/';
  $files = $wp_filesystem->dirlist( $dir );

  $answer = '';

  if ( $files ):
    foreach( $files as $file ):
      $answer .= '<tr>'.PHP_EOL;
      $answer .= '  <td class="row-title"><input type="checkbox" name="files[]" value="' . $file['name'] . '"></td>'.PHP_EOL;
      $answer .= '  <td>' . $file['name'] . '</td>'.PHP_EOL;
      $answer .= '  <td><a href="' . wp_upload_dir()['baseurl'] . '/yml-files/' . $file['name'] . '" target="_blank">Просмотреть файл</a></td>'.PHP_EOL;
      $answer .= '</tr>'.PHP_EOL;
    endforeach;
  endif;

  echo $answer;

  wp_die();
}