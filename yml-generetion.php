<?php

/**
 * Plugin Name: YML Generation
 * Plugin URI: http://localhost
 * Description: YML-generation file
 * Version: 1.2
 * Author: Mishalov Pavel
 * Author URI: http://localhost
 * Domain Path: /languages
 * WC requires at least: 2.4
 * WC tested up to: 2.6.6
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
  exit;
}

define('YML_PLUGIN_PATH', plugin_dir_path( __FILE__ ));
define('YML_PLUGIN_URL', plugins_url( '/assets', __FILE__ ));
define('YML_PLATFORM', 'WordPress');
define('PLATFORM_VERSION', get_bloginfo( 'version' ));
define('SITE_URL', get_bloginfo( 'url' ));
define('PLUGIN_AGENCY_NAME', 'YML Generetion plugin');
define('PLUGIN_AGENCY_EMAIL', 'ekonomikal@mail.ru');

final class YML_Generation
{
  
  function __construct()
  {

  }

  static function activate_plugin()
  {
    update_option(
      'yml_default_shop_option',
      array(
        'shop_name'      => get_bloginfo( 'name' ),
        'company_name'   => get_bloginfo( 'name' ),
        'site_url'       => get_bloginfo( 'url' ),
        'currencies'     =>
          array(
            '0'   => array(
                      'currency' => 'RUR',
                      'rate'     => 'MyRate',
                      'myRate'   => 1,
                    ),
            ),
        'deliveryShop'   =>
          array(
            '0'      => array(
                        'cost'     => '350',
                        'days'     => '2',
                        'order'    => '18',
                      ),
          ),
      )
    );
/*
    update_option(
      'yml_default_order_option',
      array(
        'galary_summary'   => 10,
        'type'             => 
          array(
            'Simplified' => 
              array(
                'name'       => '',
                'model'      => '',
                'vendor'     => '',
                'vendorCode' => '',
              ),
          ),
        'offer'          =>
          array(
            'cbid'         => 'none',
            'bid'          => 'none',
            'fee'          => 'none',
          ),
        'delivery'       => 'false',
        'delivery-options' =>
          array(

          ),
      )
    );*/
  }

  static function deactivate_plugin()
  {
  
  }

  static function plugin_run()
  {
    if( !class_exists( 'ShopPage' ) ):
      include_once( YML_PLUGIN_PATH . 'classes/pages/ShopPage.php' );
    endif;

    if( !class_exists( 'ProductPage' ) ):
      include_once( YML_PLUGIN_PATH . 'classes/pages/ProductPage.php' );
    endif;

    if( !class_exists( 'YMLProductMetaBox' ) ):
      include_once( YML_PLUGIN_PATH . 'classes/product/productMetaBox.php' );
    endif;

    if( !class_exists( 'GenerationPage' ) ):
      include_once( YML_PLUGIN_PATH . 'classes/pages/GenerationPage.php' );
    endif;

    if( file_exists( YML_PLUGIN_PATH . 'assets/ajax/ajax.php' ) ):
      include_once( YML_PLUGIN_PATH . 'assets/ajax/ajax.php' );
    endif;

    ShopPage::run();
    ProductPage::run();
    YMLProductMetaBox::run();
    GenerationPage::run();
  }
}

register_activation_hook(
  __FILE__,
  array(
    'YML_Generation',
    'activate_plugin'
  )
);

register_deactivation_hook(
  __FILE__,
  array(
    'YML_Generation',
    'deactivate_plugin'
  )
);

YML_Generation::plugin_run();