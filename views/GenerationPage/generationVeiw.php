<br>
<br>
<br>
<br>
<br>

<?php

  if (empty($wp_filesystem)) {
      require_once (ABSPATH . '/wp-admin/includes/file.php');
      WP_Filesystem();
      global $wp_filesystem;
  }

  $dir = wp_upload_dir()['basedir'] . '/yml-files/';
  $files = $wp_filesystem->dirlist( $dir );
?>

<table class="widefat">
  <thead>
    <tr>
      <th class="row-title id"><input type="checkbox" onClick="toggle(this)"></th>
      <th class="row-title name">File name</th>
      <th class="row-title link">Action</th>
    </tr>
  </thead>
  <tbody id="yml-generate-files">
<?php

  if ( $files ):
    foreach( $files as $file ):

?>
  <tr>
    <td class="row-title"><input type="checkbox" name="files[]" value="<?=$file['name'];?>"></td>
    <td><?=$file['name'];?></td>
    <td><a href="<?= wp_upload_dir()['baseurl'] . '/yml-files/' . $file['name'] ?>" target="_blank">Просмотреть файл</a></td>
  </tr>

<?php 
  endforeach;
  endif; 
?>

  </tbody>
</table>

<button type="button" class="button button-primary hide-if-no-js" name="yml-generate" id="yml-generate">Генерировать файл</button>
<button type="button" class="button button-primary hide-if-no-js" name="yml-generate-clear" id="yml-generate-clear">Удалить выбранные файлы</button>

<script>
  jQuery(document).ready(function($) {

    $('button#yml-generate').click(
      function(){
        $.ajax({
                  method  : 'POST',
                  url     : ajaxurl,
                  data    : { action: 'add_yml_generation_file' },
                  beforeSend : function() {
                                  $('button#yml-generate').append( '...' );
                                },
                  success : function(response) {
                                  $('button#yml-generate').html('Генерировать файл');
                                  $('tbody#yml-generate-files').html(response);
                              },
              });
      });

    $('button#yml-generate-clear').click(
      function(){
        files = [];
        $('tbody#yml-generate-files input:checkbox:checked').each(
          function(index){
            files.push( $(this).val() );
          }
        );
        $.ajax({
                  method  : 'POST',
                  url     : ajaxurl,
                  data    : { action: 'remove_yml_generation_files', yml_files : files },
                  beforeSend : function() {
                                  $('button#yml-generate-clear').append( '...' );
                                },
                  success : function(response) {
                                  $('button#yml-generate-clear').html('Удалить выбранные файлы');
                                  $('tbody#yml-generate-files').html(response);
                              },
              });
      });
  });
</script>