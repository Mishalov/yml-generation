<div id="allCurrencies">

	<?php
		$numCurresy = 0;
		foreach ($options['currencies'] as $value):
	?>

	<div class="allCurrencies" data-num="<?= $numCurresy ?>">
		<section class="currency">
			<select id="<?= $args[ 'label_for' ] . $args[ 'labelCurrency' ] . $numCurresy ?>"
					name="yml_shop_settings[<?= $args[ 'label_for' ] ?>][<?= $numCurresy ?>][<?= $args[ 'labelCurrency' ] ?>]">

				<?php foreach( $args[ 'optionCurrency' ] as $key_arr => $value_arr ) : ?>
					<option value="<?= $key_arr; ?>" <?php if ( $value[ $args[ 'labelCurrency' ] ] == $key_arr ) echo 'selected="selected"'; ?>>
						<?= $value_arr; ?>
					</option>
				<?php endforeach; ?>

			</select>

			<p class="description">
				<?= $args[ 'descriptionCurrency' ]; ?>
			</p>

		</section>
		<section class="Rate">
			<select id="<?= $args[ 'label_for' ] . $args[ 'labelRate' ] . $numCurresy ?>"
					name="yml_shop_settings[<?= $args[ 'label_for' ] ?>][<?= $numCurresy ?>][<?= $args[ 'labelRate' ] ?>]" onChange="addMyRate(<?= $numCurresy ?>, '<?= $args[ 'label_for' ] ?>', '<?= $args[ 'labelRate' ] ?>');">

				<?php foreach( $args[ 'optionRate' ] as $key_arr => $value_arr ) : ?>
					<option value="<?= $key_arr; ?>" <?php if ( $value[ $args[ 'labelRate' ] ] == $key_arr ) echo 'selected'; ?>>
						<?= $value_arr; ?>
					</option>
				<?php endforeach; ?>

			</select>

			<input type="number" name="yml_shop_settings[<?= $args[ 'label_for' ] ?>][<?= $numCurresy ?>][<?= $args[ 'labelMyRate' ] ?>]" min="0" step="0.01" style="<?php if($value[ $args[ 'labelRate' ] ] != 'MyRate') { echo 'display: none;'; } ?>" value="<?= $value['myRate'] ?>"  data-num="<?= $numCurresy ?>">

			<p class="description">
				<?= $args[ 'descriptionRate' ]; ?>
			</p>
		</section>
	<button class="plusCurrency" onClick="deleteCurrencySection(<?= $numCurresy ?>); return false;">X</button>
	</div>

	<?php
		$numCurresy++;
		endforeach;
	?>

</div>
<button class="plusCurrency" onClick="addCurrencySection('<?= $args['label_for'] ?>', '<?= $args['labelCurrency']?>', '<?= $numCurresy ?>', '<?= $args['labelMyRate'] ?>', '<?= $args['descriptionRate'] ?>', '<?= $args['labelRate'] ?>', '<?= $args[ 'descriptionCurrency' ]; ?>'); return false;" type="button" style="clear: both">+</button>