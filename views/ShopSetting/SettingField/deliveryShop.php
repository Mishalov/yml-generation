<div id="allDeliveryShop">

<?php
	$numDelivery = 0;
	foreach ($options['deliveryShop'] as $key => $value):
?>

	<section class="deliveryShop" data-num="<?= $numDelivery ?>">
		<div class="inputDelivery">
			<input type="number" class="deliveryInput" name="yml_shop_settings[<?= $args[ 'label_for' ] ?>][<?= $numDelivery ?>][<?= $args[ 'labelCost' ] ?>]" min="0" value="<?= $value[$args['labelCost']] ?>">
			<p class="description"><?= $args['descriptionCost'] ?></p>
		</div>
		<div class="inputDelivery">
			<input type="number" class="deliveryInput" name="yml_shop_settings[<?= $args[ 'label_for' ] ?>][<?= $numDelivery ?>][<?= $args[ 'labelDays' ] ?>]" min="0" max="32" value="<?= $value[$args['labelDays']] ?>">
			<p class="description"><?= $args['descriptionDays'] ?></p>
		</div>
		<div class="inputDelivery">
			<input type="number" class="deliveryInput" name="yml_shop_settings[<?= $args[ 'label_for' ] ?>][<?= $numDelivery ?>][<?= $args[ 'labelOrderBefore' ] ?>]" min="0" max="24" value="<?= $value[$args['labelOrderBefore']] ?>">
			<p class="description"><?= $args['descriptionOrder'] ?></p>
		</div>
		<button class="plusDelivery" type="button" onClick="deleteDeliverySection(<?= $numDelivery ?>); return false;" style="clear: none;">X</button>
	</section>
<?php
	$numDelivery++;
	endforeach;
?>
</div>
<button class="plusDelivery" type="button" onClick="addDeliverySection('<?= $args[ 'label_for' ] ?>', '<?= $args[ 'labelCost' ] ?>', '<?= $args['descriptionCost'] ?>', '<?= $args[ 'labelDays' ] ?>', '<?= $args['descriptionDays'] ?>', '<?= $args[ 'labelOrderBefore' ] ?>', '<?= $args['descriptionOrder'] ?>'); return false;" style="clear: none;">+</button>