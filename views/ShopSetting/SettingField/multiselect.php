<?php

$select_array = [];
if ( isset( $options[ $args[ 'label_for' ] ] ) )
	$select_array = $options[ $args[ 'label_for' ] ];

?>

<select id="<?= $args[ 'label_for' ] ?>"
		name="yml_shop_settings[<?= $args[ 'label_for' ] ?>][]"
		multiple style="padding: 5px">
	
		<?php foreach ( get_categories( [ 'taxonomy' => 'product_cat', 'parent' => 0 ] ) as $category ) : ?>
	<option value="<?= $category->cat_ID; ?>" <?php if ( in_array( $category->cat_ID, $select_array ) ) echo "selected"; ?>>
		<?= $category->name; ?>
	</option>

		<?php foreach ( get_categories( [ 'taxonomy' => 'product_cat', 'parent' => $category->cat_ID ] ) as $category2 ) : ?>
	<option value="<?= $category2->cat_ID; ?>" <?php if ( in_array( $category2->cat_ID, $select_array ) ) echo "selected"; ?>>
		&#160;&#160;&#8594;<?= $category2->name; ?>
	</option>

		<?php foreach ( get_categories( [ 'taxonomy' => 'product_cat', 'parent' => $category2->cat_ID ] ) as $category3 ) : ?>
	<option value="<?= $category3->cat_ID; ?>" <?php if ( in_array( $category3->cat_ID, $select_array ) ) echo "selected"; ?>>
		&#160;&#160;&#160;&#160;&#160;&#160;&#8594;<?= $category3->name; ?>
	</option>
		<?php endforeach; ?>

		<?php endforeach; ?>

		<?php endforeach; ?>

</select>
