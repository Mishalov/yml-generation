<form class="adminForm" action="options.php" method="post">
<?php

	settings_fields( 'shop_options' );
	
	do_settings_sections( 'shop_setting' );
	
	submit_button('Save Settings');

   wp_enqueue_script('ymlShopPageScript', YML_PLUGIN_URL . '/js/adminShopPage.js');
   wp_enqueue_style('ymlShopPageStyle', YML_PLUGIN_URL . '/css/adminShopPage.css');

   echo "<pre>";
   print_r(get_option('yml_shop_settings'));
?>

</form>