<?php

    woocommerce_wp_checkbox(
      array(
        'label'       => 'Не выводить данный товар',
        'id'          => '_yml_veiw_product',
        'required'    => true,
        'desc_tip'    => true,
        'description' => 'description',
      )
    );

    woocommerce_wp_checkbox(
      array(
        'label'       => 'Задавать настройки самостоятельно',
        'id'          => '_yml_default_option',
        'required'    => true,
        'desc_tip'    => true,
        'description' => 'description',
      )
    );

    //type
		woocommerce_wp_select(
			array(
				'label'       => 'Тип описания товара',
				'id'          => '_yml_type',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'easy'         => 'Упрощенный тип',
										'vendor.model' => 'Произвольный тип',
										'book'         => 'Специальный тип (книги)',
										'audiobook'    => 'Специальный тип (аудиокниги)',
										'artist.title' => 'Специальный тип (Музыкальная и видеопродукция)',
										'medicine'     => 'Специальный тип (лекарства)',
										'event-ticket' => 'Специальный тип (билеты на мероприятие)',
										'tour'         => 'Специальный тип (туры)',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_type_yml_option(this.value);',
                  ),
			)
		);

    //currency
		woocommerce_wp_select(
			array(
				'label'       => 'Валюта продажи товара',
				'id'          => '_yml_currencyId',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'RUR' => 'Рубль',
										'USD' => 'Доллар',
										'EUR' => 'Евро',
										'UAH' => 'Гривна',
										'KZT' => 'Тенге',
										'BYN' => 'Беларуский рубль',
									),
			)
		);

    //description
		woocommerce_wp_select(
			array(
				'label'       => 'Описание товарного предложения',
				'id'          => '_yml_description',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'        => 'Не отображать',
										'description' => 'Использовать текст продукта',
										'excerpt'     => 'Использовать краткое описание товара',
										'myInput'     => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_description(this);',
                    'data-lable' => 'description',
                  ),
			)
		);
		
		$specifications = get_post_meta( $post->ID, '_yml_description_myInput', true );
		wp_editor( htmlspecialchars_decode( $specifications ), '_yml_description_myInput', array('style' => array('width' => '90%', )) );

    //sales-notes
		woocommerce_wp_select(
			array(
				'label'       => 'Дополнительная информация о товарном предложении',
				'id'          => '_yml_sales_notes',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'excerpt'   => 'Использовать краткое описание товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'sales_notes',
                  ),
				)
		);

    woocommerce_wp_textarea_input(
      array(
        'label'       => '',
        'id'          => '_yml_sales_notes_myInput',
        'desc_tip'    => true,
        'description' => 'description',
        'placeholder' => ''
      )
    );

    //vendor
		woocommerce_wp_select(
			array(
				'label'       => 'Производитель товара',
				'id'          => '_yml_vendor',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'vendor',
                  ),
				)
		);

		global $wpdb;
		$attribute_array = $wpdb->get_results("SELECT attribute_name AS attr_key, attribute_label AS attr_value
								 FROM $wpdb->prefix" . "woocommerce_attribute_taxonomies", ARRAY_N );
		foreach ($attribute_array as $key => $value) {
			$attribute[$value[0]] = $value[1];
		}

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_vendor_attribute',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_vendor_myInput',
				'desc_tip'    => true,
				'description' => 'description',
				'placeholder' => 'Производитель',
			)
		);

    //model
		woocommerce_wp_select(
			array(
				'label'       => 'Модель товара',
				'id'          => '_yml_model',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'model',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_model_attribute',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_model_myInput',
				'desc_tip'    => true,
				'description' => 'description',
				'placeholder' => 'Модель'
			)
		);

    //vendorCode
		woocommerce_wp_select(
			array(
				'label'       => 'Код производителя',
				'id'          => '_yml_vendorCode',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'vendorCode',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_vendorCode_attribute',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_vendorCode_myInput',
				'desc_tip'    => true,
				'description' => 'description',
				'placeholder' => 'код производителя'
			)
		);

    //typePrefix
		woocommerce_wp_select(
			array(
				'label'       => 'Тип товарного предложения',
				'id'          => '_yml_typePrefix',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'typePrefix',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_typePrefix_attribute',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_typePrefix_myInput',
				'desc_tip'    => true,
				'description' => 'description',
				'placeholder' => 'тип предложения'
			)
		);

    //author
		woocommerce_wp_select(
			array(
				'label'       => 'Автор',
				'id'          => '_yml_author',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'author',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_author_attribute',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_author_myInput',
				'desc_tip'    => true,
				'description' => 'description',
				'placeholder' => 'автор'
			)
		);

    //publisher
		woocommerce_wp_select(
			array(
				'label'       => 'Издательство',
				'id'          => '_yml_publisher',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'publisher',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_publisher_attribute',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_publisher_myInput',
				'desc_tip'    => true,
				'description' => 'description',
				'placeholder' => 'издательство'
			)
		);

    //series
		woocommerce_wp_select(
			array(
				'label'       => 'Серия',
				'id'          => '_yml_series',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'series',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_series_attribute',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_series_myInput',
				'desc_tip'    => true,
				'description' => 'description',
				'placeholder' => 'серия произведений'
			)
		);

    //year
		woocommerce_wp_select(
			array(
				'label'       => 'Год произведения',
				'id'          => '_yml_year',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'year',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_year_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_year_myInput',
				'desc_tip'    => true,
				'type'        => 'number',
				'description' => 'description',
				'placeholder' => 'год произведения'
			)
		);

    //ISBN
		woocommerce_wp_select(
			array(
				'label'       => 'ISBN',
				'id'          => '_yml_ISBN',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'ISBN',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_ISBN_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_ISBN_myInput',
				'desc_tip'    => true,
				'description' => 'description',
				'placeholder' => 'номер книжного издания'
			)
		);

    //volume
		woocommerce_wp_select(
			array(
				'label'       => 'Количество томов',
				'id'          => '_yml_volume',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'volume',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_volume_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_volume_myInput',
				'desc_tip'    => true,
				'type'        => 'number',
				'description' => 'description',
				'placeholder' => 'количество томов'
			)
		);

    //part
		woocommerce_wp_select(
			array(
				'label'       => 'Номер тома',
				'id'          => '_yml_part',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'part',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_part_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_part_myInput',
				'desc_tip'    => true,
				'type'        => 'number',
				'description' => 'description',
				'placeholder' => 'номер тома'
			)
		);

    //language
		woocommerce_wp_select(
			array(
				'label'       => 'Язык произведения',
				'id'          => '_yml_language',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'language',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_language_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_language_myInput',
				'desc_tip'    => true,
				'description' => 'description',
				'placeholder' => 'язык произведения'
			)
		);

    //binding
		woocommerce_wp_select(
			array(
				'label'       => 'Переплет',
				'id'          => '_yml_binding',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'binding',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_binding_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_binding_myInput',
				'desc_tip'    => true,
				'description' => 'description',
				'placeholder' => 'переплет'
			)
		);

    //page_extent
		woocommerce_wp_select(
			array(
				'label'       => 'Количество страниц',
				'id'          => '_yml_page_extent',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'page_extent',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_page_extent_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_page_extent_myInput',
				'desc_tip'    => true,
				'type'        => 'number',
				'description' => 'description',
				'placeholder' => 'количество страниц'
			)
		);

		//performed_by
		woocommerce_wp_select(
			array(
				'label'       => 'Исполнитель аудиокниги',
				'id'          => '_yml_performed_by',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'performed_by',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_performed_by_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_performed_by_myInput',
				'desc_tip'    => true,
				'type'        => 'text',
				'description' => 'description',
				'placeholder' => 'исполнитель'
			)
		);

		//storage
		woocommerce_wp_select(
			array(
				'label'       => 'Носитель аудиокниги',
				'id'          => '_yml_storage',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'storage',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_storage_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_storage_myInput',
				'desc_tip'    => true,
				'type'        => 'text',
				'description' => 'description',
				'placeholder' => 'носитель'
			)
		);

		//format

		woocommerce_wp_select(
			array(
				'label'       => 'Формат аудиокниги',
				'id'          => '_yml_format',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'format',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_format_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_format_myInput',
				'desc_tip'    => true,
				'type'        => 'text',
				'description' => 'description',
				'placeholder' => 'формат'
			)
		);

		//recording_length

		woocommerce_wp_select(
			array(
				'label'       => 'Время озвучивания аудиокниги',
				'id'          => '_yml_recording_length',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'recording_length',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_recording_length_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_recording_length_myInput',
				'desc_tip'    => true,
				'type'        => 'number',
				'description' => 'description',
				'placeholder' => 'Время озвучивания'
			)
		);

		//artist

		woocommerce_wp_select(
			array(
				'label'       => 'Исполнитель',
				'id'          => '_yml_artist',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'artist',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_artist_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_artist_myInput',
				'desc_tip'    => true,
				'type'        => 'number',
				'description' => 'description',
				'placeholder' => 'исполнитель'
			)
		);

		//media

		woocommerce_wp_select(
			array(
				'label'       => 'Носитель',
				'id'          => '_yml_media',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'media',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_media_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_media_myInput',
				'desc_tip'    => true,
				'type'        => 'text',
				'description' => 'description',
				'placeholder' => 'носитель'
			)
		);

		//starring

		woocommerce_wp_select(
			array(
				'label'       => 'Актеры',
				'id'          => '_yml_starring',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'starring',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_starring_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_textarea_input(
			array(
				'label'       => '',
				'id'          => '_yml_starring_myInput',
				'desc_tip'    => true,
				'description' => 'description',
				'placeholder' => 'Актеры'
			)
		);

		//director

		woocommerce_wp_select(
			array(
				'label'       => 'Режиссер',
				'id'          => '_yml_director',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'director',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_director_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_director_myInput',
				'desc_tip'    => true,
				'type'        => 'text',
				'description' => 'description',
				'placeholder' => 'режиссер'
			)
		);

		//originalName

		woocommerce_wp_select(
			array(
				'label'       => 'Оригинальное название',
				'id'          => '_yml_originalName',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'originalName',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_originalName_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_originalName_myInput',
				'desc_tip'    => true,
				'type'        => 'text',
				'description' => 'description',
				'placeholder' => 'название'
			)
		);

		//country

		woocommerce_wp_select(
			array(
				'label'       => 'Страна произведения(тура)',
				'id'          => '_yml_country',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'country',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_country_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_country_myInput',
				'desc_tip'    => true,
				'type'        => 'text',
				'description' => 'description',
				'placeholder' => 'страна'
			)
		);

		//ETK

		woocommerce_wp_select(
			array(
				'label'       => 'Код лекарства в Едином городском классификаторе',
				'id'          => '_yml_ETK',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'ETK',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_ETK_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_ETK_myInput',
				'desc_tip'    => true,
				'type'        => 'text',
				'description' => 'description',
				'placeholder' => 'Код лекарства'
			)
		);

		//place

		woocommerce_wp_select(
			array(
				'label'       => 'Место проведения',
				'id'          => '_yml_place',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'place',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_place_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_place_myInput',
				'desc_tip'    => true,
				'type'        => 'text',
				'description' => 'description',
				'placeholder' => 'место проведения'
			)
		);

		//hall

		woocommerce_wp_select(
			array(
				'label'       => 'Зал проведения',
				'id'          => '_yml_hall',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'hall',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_hall_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_hall_myInput',
				'desc_tip'    => true,
				'type'        => 'text',
				'description' => 'description',
				'placeholder' => 'Зал проведения'
			)
		);

		//hall_part

		woocommerce_wp_select(
			array(
				'label'       => 'Ряд и место в зале',
				'id'          => '_yml_hall_part',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'hall_part',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_hall_part_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_hall_part_myInput',
				'desc_tip'    => true,
				'type'        => 'text',
				'description' => 'description',
				'placeholder' => 'место в зале'
			)
		);

		//date

		woocommerce_wp_select(
			array(
				'label'       => 'Дата и время сеанса',
				'id'          => '_yml_date',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'date',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_date_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_date_myInput',
				'desc_tip'    => true,
				'type'        => 'text',
				'description' => 'description',
				'placeholder' => 'время сеанса'
			)
		);

		//worldRegion

		woocommerce_wp_select(
			array(
				'label'       => 'Часть света тура',
				'id'          => '_yml_worldRegion',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'worldRegion',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_worldRegion_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_worldRegion_myInput',
				'desc_tip'    => true,
				'type'        => 'text',
				'description' => 'description',
				'placeholder' => 'часть света тура'
			)
		);

		//region

		woocommerce_wp_select(
			array(
				'label'       => 'Курорт или город тура',
				'id'          => '_yml_region',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'region',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_region_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_region_myInput',
				'desc_tip'    => true,
				'type'        => 'text',
				'description' => 'description',
				'placeholder' => 'город тура'
			)
		);

		//days

		woocommerce_wp_select(
			array(
				'label'       => 'Количество дней тура',
				'id'          => '_yml_days',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'days',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_days_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_days_myInput',
				'desc_tip'    => true,
				'type'        => 'number',
				'description' => 'description',
				'placeholder' => 'количество дней'
			)
		);

		//dataTour

		woocommerce_wp_select(
			array(
				'label'       => 'Дата заезда',
				'id'          => '_yml_dataTour',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'dataTour',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_dataTour_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_dataTour_myInput',
				'desc_tip'    => true,
				'type'        => 'text',
				'description' => 'description',
				'placeholder' => 'дата заезда'
			)
		);

		//hotel_stars

		woocommerce_wp_select(
			array(
				'label'       => 'Количество звезд отеля',
				'id'          => '_yml_hotel_stars',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'hotel_stars',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_hotel_stars_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_hotel_stars_myInput',
				'desc_tip'    => true,
				'type'        => 'number',
				'description' => 'description',
				'placeholder' => 'количество звезд'
			)
		);

		//transport

		woocommerce_wp_select(
			array(
				'label'       => 'Транспорт отправки',
				'id'          => '_yml_transport',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'transport',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_transport_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_transport_myInput',
				'desc_tip'    => true,
				'type'        => 'text',
				'description' => 'description',
				'placeholder' => 'транспорт'
			)
		);

		//cbid

		woocommerce_wp_select(
			array(
				'label'       => 'Размер ставки на карточке модели',
				'id'          => '_yml_cbid',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'cbid',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_cbid_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_cbid_myInput',
				'desc_tip'    => true,
				'type'        => 'number',
				'description' => 'description',
				'placeholder' => 'размер ставки',
            'custom_attributes'  => array(
                    'min'        => '0',
                    'step'       => '0.01',
                  ),
			)
		);

		//bid

		woocommerce_wp_select(
			array(
				'label'       => 'Размер ставки на остальных местах размещения',
				'id'          => '_yml_bid',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'bid',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_bid_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_bid_myInput',
				'desc_tip'    => true,
				'type'        => 'number',
				'description' => 'description',
				'placeholder' => 'размер ставки',
        'custom_attributes'  => array(
                    'min'        => '0',
                    'step'       => '0.01',
                  ),
			)
		);

		//fee

		woocommerce_wp_select(
			array(
				'label'       => 'Размер комиссии на товарное предложение, участвующее в программе «Заказ на Маркете»',
				'id'          => '_yml_fee',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'fee',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_fee_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_fee_myInput',
				'desc_tip'    => true,
				'type'        => 'number',
				'description' => 'description',
				'placeholder' => 'размер комиссии',
        		'custom_attributes'  => array(
                    'min'        => '0',
                    'step'       => '0.01',
                  ),
			)
		);

		//country_of_origin

		woocommerce_wp_select(
			array(
				'label'       => 'Страна производитель продукции',
				'id'          => '_yml_country_of_origin',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'country_of_origin',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_country_of_origin_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_country_of_origin_myInput',
				'desc_tip'    => true,
				'type'        => 'text',
				'description' => 'description',
				'placeholder' => 'страна'
			)
		);

		//barcode

		woocommerce_wp_select(
			array(
				'label'       => 'Штрихкод товара',
				'id'          => '_yml_barcode',
				'required'    => true,
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => array(
										'none'      => 'Не отображать',
										'attribute' => 'Использовать атрибут товара',
										'myInput'   => 'Задавать самостоятельно',
									),
        'custom_attributes'  => array(
                    'onchange'   => 'mis_views_yml_option(this);',
                    'data-lable' => 'barcode',
                  ),
				)
		);

		woocommerce_wp_select(
			array(
				'label'       => '',
				'id'          => '_yml_barcode_attribute',
				'desc_tip'    => true,
				'description' => 'description',
				'options'     => $attribute,
				)
		);

		woocommerce_wp_text_input(
			array(
				'label'       => '',
				'id'          => '_yml_barcode_myInput',
				'desc_tip'    => true,
				'type'        => 'text',
				'description' => 'description',
				'placeholder' => 'штрихкод'
			)
		);

    //ExtraOptions
?>
<hr>
<p style="text-align: center;">Дополнительные параметры товара</p>
<?php
    woocommerce_wp_checkbox(
      array(
        'label'       => 'Возможность курьерской доставки товара',
        'id'          => '_yml_delivery',
        'required'    => true,
        'desc_tip'    => true,
        'description' => 'Возможность курьерской доставки товара',
      )
    );
    
    woocommerce_wp_checkbox(
      array(
        'label'       => 'Возможность самовывоза из пунктов выдачи',
        'id'          => '_yml_pickup',
        'required'    => true,
        'desc_tip'    => true,
        'description' => 'Возможность самовывоза из пунктов выдачи',
      )
    );
    
    woocommerce_wp_checkbox(
      array(
        'label'       => 'Возможность купить товар в розницу',
        'id'          => '_yml_store',
        'required'    => true,
        'desc_tip'    => true,
        'description' => 'description',
      )
    );
    
    woocommerce_wp_checkbox(
      array(
        'label'       => 'Товар имеет официальную гарантию производителя',
        'id'          => '_yml_manufacturer_warranty',
        'required'    => true,
        'desc_tip'    => true,
        'description' => 'description',
      )
    );
    
    woocommerce_wp_checkbox(
      array(
        'label'       => 'Товар, имеет отношение к удовлетворению сексуальных потребностей',
        'id'          => '_yml_adult',
        'required'    => true,
        'desc_tip'    => true,
        'description' => 'description',
      )
    );
    
    woocommerce_wp_checkbox(
      array(
        'label'       => 'Товар имеет возможность к дистанционной торговле',
        'id'          => '_yml_cpa',
        'required'    => true,
        'desc_tip'    => true,
        'description' => 'description',
      )
    );
    
    woocommerce_wp_checkbox(
      array(
        'label'       => 'Указывать вес товара',
        'id'          => '_yml_weight',
        'required'    => true,
        'desc_tip'    => true,
        'description' => 'description',
      )
    );
    
    woocommerce_wp_checkbox(
      array(
        'label'       => 'Указывать габариты товара',
        'id'          => '_yml_dimensions',
        'required'    => true,
        'desc_tip'    => true,
        'description' => 'description',
      )
    );
    
    woocommerce_wp_checkbox(
      array(
        'label'       => 'Премьера',
        'id'          => '_yml_is_premiere',
        'required'    => true,
        'desc_tip'    => true,
        'description' => 'description',
      )
    );
    
    woocommerce_wp_checkbox(
      array(
        'label'       => 'Детское мероприятие',
        'id'          => '_yml_is_kids',
        'required'    => true,
        'desc_tip'    => true,
        'description' => 'description',
      )
    );