<div id="product_default_<?= $args['label_for'] ?>">
<?php 
	foreach ($args['get'] as $key => $value):
		$checked = '';
		if( isset($options[$args['label_for']]['get']) && $key == $options[$args['label_for']]['get'] ):
			$checked .= 'checked="checked"';
		endif;
?>
	<input name="yml_default_product_settings[<?= $args['label_for'] ?>][get]" value="<?= $key ?>" <?= $checked ?> type="radio">
	<?= $value ?>

<?php
	endforeach;
?>

<p>
	<input type="number" name="yml_default_product_settings[<?= $args['label_for'] ?>][year]" min="0" value="<?= $options[ $args['label_for']]['year'] ?>">
	Года
	<input type="number" name="yml_default_product_settings[<?= $args['label_for'] ?>][month]" min="0" max="11" value="<?= $options[ $args['label_for']]['month'] ?>">
	Месяца
	<input type="number" name="yml_default_product_settings[<?= $args['label_for'] ?>][day]" min="0" max="30" value="<?= $options[ $args['label_for']]['day'] ?>">
	Дни
	<input type="number" name="yml_default_product_settings[<?= $args['label_for'] ?>][houre]" min="0" max="23" value="<?= $options[ $args['label_for']]['houre'] ?>">
	Часы
</p>
</div>