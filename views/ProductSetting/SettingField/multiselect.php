<?php

$select_array = [];

if ( isset( $options[ $args[ 'label_for' ] ] ) ):
	$select_array = $options[ $args[ 'label_for' ] ];
endif;

global $wpdb;

$attribute_array = $wpdb->get_results("SELECT attribute_name AS attr_key, attribute_label AS attr_value
									 FROM $wpdb->prefix" . "woocommerce_attribute_taxonomies", ARRAY_N );
?>

<div id="product_default_<?= $args['label_for'] ?>">
	<select id="<?= $args[ 'label_for' ] ?>"
			name="yml_default_product_settings[<?= $args[ 'label_for' ] ?>][]"
			multiple style="padding: 5px">
		
			<?php foreach ( $attribute_array as $key => $attribute ) : ?>
		<option value="<?= $attribute[0].'|'.$attribute[1] ?>" <?php if ( in_array( $attribute[0], $select_array ) ) echo "selected"; ?>>
			<?= $attribute[1] ?>
		</option>
			<?php endforeach; ?>

	</select>
	<p class="description"><?= $args['description'] ?></p>

</div>