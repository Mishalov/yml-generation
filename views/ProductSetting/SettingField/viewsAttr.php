<div id="product_default_<?= $args['label_for'] ?>">
<?php 
	foreach ($args['get'] as $key => $value):
		$checked = '';
		if( isset($options[$args['label_for']]['get']) && $key == $options[$args['label_for']]['get'] ):
			$checked .= 'checked="checked"';
		endif;
?>
	<input name="yml_default_product_settings[<?= $args['label_for'] ?>][get]" value="<?= $key ?>" <?= $checked ?> type="radio" onchange="onChangeInput(this, '<?= $args['label_for'] ?>');">
	<lable for=""><?= $value ?></lable>

<?php
	endforeach;
	global $wpdb;

	$attribute_array = $wpdb->get_results("SELECT attribute_name AS attr_key, attribute_label AS attr_value
								 FROM $wpdb->prefix" . "woocommerce_attribute_taxonomies", ARRAY_N );

	$attribute_display = 'style="display: none;"';
	$myInput_display = 'style="display: none;"';
	if(isset($options[$args['label_for']]['get']) && $options[$args['label_for']]['get'] == 'attribute'){
		$attribute_display = '';
	}elseif(isset($options[$args['label_for']]['get']) && $options[$args['label_for']]['get'] == 'myInput'){
		$myInput_display = '';
	}
?>

<p <?= $attribute_display ?> data-lable="<?= $args[ 'label_for' ].'-attribute' ?>">
	<select name="yml_default_product_settings[<?= $args[ 'label_for' ] ?>][attribute]" >
			<?php foreach( $attribute_array as $key => $value ):
					$checked = '';
					if( $options[$args['label_for']]['attribute'] == $value[0] ):
						$checked .= 'selected="selected"';
					endif;
			?>
		<option value="<?= $value[0] ?>" <?= $checked ?>>
			<?= $value[1] ?>
		</option>
			<?php endforeach; ?>
	</select>
</p>

<?php
	$myInput = '';
	if(isset($options[ $args['label_for'] ]['myInput'])): 
		$myInput = $options[ $args['label_for'] ]['myInput'];
	endif;
	if($args['typeMyInput'] == 'textarea'){
?>
<p <?= $myInput_display ?> data-lable="<?= $args[ 'label_for' ].'-myInput' ?>">
	<textarea name="yml_default_product_settings[<?= $args[ 'label_for' ] ?>][myInput]"><?= $myInput ?></textarea>
</p>
<?php
	}elseif($args['typeMyInput'] == 'select'){
		if(file_exists($file_name = YML_PLUGIN_PATH . 'assets/country.txt' )):
			$file = file( $file_name );
?>
<p <?= $myInput_display ?> data-lable="<?= $args[ 'label_for' ].'-myInput' ?>">
<select name="yml_default_product_settings[<?= $args[ 'label_for' ] ?>][myInput]">
		<?php foreach( $file as $value ):
				$checked = '';
				if( $options[$args['label_for']]['myInput'] == trim($value) ):
					$checked .= 'selected="selected"';
				endif;
		?>
	<option value="<?= trim($value) ?>" <?= $checked ?>>
		<?= trim($value) ?>
	</option>
		<?php endforeach; ?>
</select>
</p>
<?php
		endif;
	}elseif($args['typeMyInput'] == 'editor'){
?>
<div <?= $myInput_display ?> data-lable="<?= $args[ 'label_for' ].'-myInput' ?>">
	<?php
		wp_editor( htmlspecialchars_decode(
			$options[$args['label_for']]['myInput'] ),
			'yml_default_product_settings',
			array(
				'textarea_name' => 'yml_default_product_settings[' . $args[ "label_for" ] . '][myInput]',
				'editor_height' => 200,
			)
		);
	?>
</div>
<?php
	}else{
?>
<p <?= $myInput_display ?> data-lable="<?= $args[ 'label_for' ].'-myInput' ?>">
	<input name="yml_default_product_settings[<?= $args[ 'label_for' ] ?>][myInput]" value="<?= $myInput ?>" type="<?= $args['typeMyInput'] ?>" >
</p>
<?php
	}
?>
<p class="description"><?= $args['description'] ?></p>
</div>