<div id="product_default_<?= $args['label_for'] ?>">
<?php
	foreach ($args['get'] as $key => $value):
		$checked = '';
		if( isset($options[$args['label_for']][$key]) && $options[$args['label_for']][$key] == 'on'):
			$checked .= 'checked="checked"';
		endif;
?>
<p>
	<input name="yml_default_product_settings[<?= $args['label_for'] ?>][<?= $key ?>]" <?= $checked ?> type="checkbox">
	<label for="yml_default_product_settings[<?= $args['label_for'] ?>][<?= $key ?>]"><?= $value ?></label>
</p>
<?php
	endforeach;
?>
</div>