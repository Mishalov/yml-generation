<div id="product_default_<?= $args['label_for'] ?>">
<?php 
	foreach ($args['get'] as $key => $value):
		$checked = '';
		if( $key == $options[$args['label_for']] ):
			$checked .= 'checked="checked"';
		endif;
		$onchange = '';
		if( $args['label_for'] == 'type' ):
			$onchange .= 'onchange="addProductType(this);"';
		endif;

?>
<p>
	<input id="<?= $args['label_for'] . $key ?>" name="yml_default_product_settings[<?= $args['label_for'] ?>]" value="<?= $key ?>" <?= $checked ?> <?= $onchange ?> type="radio">
	<label for="<?= $args['label_for'] . $key ?>"><?= $value ?></label>
</p>

<?php
	endforeach;
?>

<p class="description"><?= $args['description'] ?></p>
</div>