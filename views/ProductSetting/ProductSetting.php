<form class="adminForm" action="options.php" method="post">
<?php

	settings_fields( 'default_product_options' );
	
	do_settings_sections( 'default_product_setting' );
	
	submit_button('Save Settings');

	wp_enqueue_script('ymlProductPageScript', YML_PLUGIN_URL . '/js/adminProductPage.js');
	wp_enqueue_style('ymlProductPageStyle', YML_PLUGIN_URL . '/css/adminProductPage.css');

	echo '<pre>';
	print_r( get_option( 'yml_default_product_settings' ) );
?>